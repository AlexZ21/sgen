#ifndef SGEN_SIGSLOT_H
#define SGEN_SIGSLOT_H

#include <sgen/common/export.h>

#include <unordered_map>
#include <functional>
#include <type_traits>
#include <utility>

namespace sgen {

template<typename... Args>
class SGEN_EXPORT Signal
{
public:
    Signal() = default;
    ~Signal() = default;

    template<typename T, typename Func>
    void connect(T *obj, Func &&func) {
        auto f = [obj, func](Args... args){
            (obj->*func)(std::forward<Args>(args)...);
        };
        m_slots.emplace(obj, f);
    }

    void connect(const std::function<void(Args...)> &slot) {
        m_slots.emplace(this, slot);
    }

    template<typename T>
    void disconnect(T *obj) {
        auto range = m_slots.equal_range(obj);
        for (auto it = range.first; it != range.second; ++it) {
            m_slots.erase(it);
        }
    }

    void disconnect() {
        m_slots.clear();
    }

//    void operator()(Args &&... args) {
//        for (auto &it : m_slots) {
//            it.second(std::forward<Args>(args)...);
//        }
//    }

//    void operator()(Args &... args) {
//        for (auto &it : m_slots) {
//            it.second(std::forward<Args>(args)...);
//        }
//    }

    void operator()(Args ... args) {
        for (auto &it : m_slots) {
            it.second(std::forward<Args>(args)...);
        }
    }


private:
    std::unordered_multimap<void *, std::function<void(Args...)>> m_slots;

};

}

#endif // SGEN_SIGSLOT_H
