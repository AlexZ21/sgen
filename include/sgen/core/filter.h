#ifndef SGEN_FILTER_H
#define SGEN_FILTER_H

#include <sgen/common/export.h>
#include <sgen/core/component.h>
#include <sgen/core/classtypeid.h>

#include <bitset>
#include <vector>

namespace sgen {

template<typename T, int32_t MASK_SIZE>
class SGEN_EXPORT Filter
{
public:
    Filter() = default;
    ~Filter() = default;

    bool doesPassFilter(const std::bitset<MASK_SIZE> &mask) const {
        for(size_t i = 0; i < m_mask.size(); ++i) {
            if(m_mask[i] == true && mask[i] == false)
                return false;
        }
        return true;
    }

    std::bitset<MASK_SIZE> mask() const {
        return m_mask;
    }

    template <class... Args>
    struct TypeList { };

    template <typename Head, typename... Args>
    static Filter make() {
        Filter filter;
        makeHelper(TypeList<Head, Args...>(), filter.m_mask);
        return filter;
    }

private:
    template <typename Head, typename... Args>
    static void makeHelper(TypeList<Head, Args...> typeList, std::bitset<MASK_SIZE> &mask) {
        (void)(typeList);
        if (!std::is_base_of<T, Head>())
            return;
        mask.set(ClassTypeId<T>::template id<Head>());
        makeHelper(TypeList<Args...>(), mask);
    }

    template <typename... Args>
    static void makeHelper(TypeList<Args...> typeList, std::bitset<MASK_SIZE> &mask) {
        (void)(typeList);
        (void)(mask);
    }

private:
    std::bitset<MASK_SIZE> m_mask;

};

}

#endif // SGEN_FILTER_H
