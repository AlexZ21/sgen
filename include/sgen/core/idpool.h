#ifndef SGEN_IDPOOL_H
#define SGEN_IDPOOL_H

#include <sgen/common/export.h>

#include <vector>
#include <stdint.h>

namespace sgen {

struct SGEN_EXPORT Id
{
    Id() : m_id(0) {}
    explicit Id(uint64_t id) : m_id(id) {}
    Id(uint32_t index, uint32_t version) : m_id(uint64_t(index) | uint64_t(version) << 32UL) {}
    Id(const Id &other) : m_id(other.m_id) {}

    bool operator == (const Id &other) const {
        return m_id == other.m_id;
    }

    bool operator != (const Id &other) const {
        return m_id != other.m_id;
    }

    Id operator=(const Id &other) {
        m_id = other.m_id;
        return *this;
    }

    operator int32_t () const {
        return static_cast<int32_t>(m_id & 0xffffffffUL);
    }

    operator uint64_t () const {
        return static_cast<uint64_t>(m_id & 0xffffffffUL);
    }

    operator bool() const {
        return m_id != 0;
    }

    uint64_t id() const { return m_id; }
    uint32_t index() const { return m_id & 0xffffffffUL; }
    uint32_t version() const { return m_id >> 32; }

private:
    uint64_t m_id;

};

static const Id INVALID_ID;

class SGEN_EXPORT IdPool
{
public:
    IdPool();
    virtual ~IdPool() = default;

    Id create();
    void remove(const Id &id);
    Id get(uint32_t index) const;

    bool valid(const Id &id) const;

    int32_t size() const;
    void clear();

private:
    uint32_t m_indexCounter;
    std::vector<uint32_t> m_versions;
    std::vector<uint32_t> m_freeList;
};

template <typename T>
class IdObjectPool : public IdPool
{
public:
    IdObjectPool() : IdPool() {}
    ~IdObjectPool() = default;

    Id create() {
        Id id = IdPool::create();
        if (id.index() >= m_objects.size())
            m_objects.resize(size());
        return id;
    }

    const T *at(const Id &id) const {
        if (!valid(id)) {
            return nullptr;
        }
        return &m_objects.at(id);
    }

    T *operator [] (const Id &id) {
        if (!valid(id)) {
            return nullptr;
        }
        return &m_objects[id];
    }

    void clear() {
        IdPool::clear();
        m_objects.clear();
    }

private:
    std::vector<T> m_objects;

};

}

#endif // SGEN_IDPOOL_H
