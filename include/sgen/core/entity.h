#ifndef SGEN_ENTITY_H
#define SGEN_ENTITY_H

#include <sgen/common/export.h>
#include <sgen/core/idpool.h>
#include <sgen/core/component.h>

#include <vector>

namespace sgen {

class World;

class SGEN_EXPORT Entity
{
    friend class World;
public:
    Entity();
    Entity(World *world, Id id);
    ~Entity();

    World *world() const;
    Id id() const { return m_id; }

    bool isValid() const;
    bool isEnabled() const;
    void enable();
    void disable();
    void destroy();

    template <typename T, typename... Args>
    T *addComponent(Args&&... args) {
        if (!std::is_base_of<Component, T>())
            return nullptr;
        T *component = new T{std::forward<Args>(args)...};
        addComponent(component, ClassTypeId<Component>::id<T>());
        return component;
    }

    template <typename T>
    void removeComponent() {
        if (!std::is_base_of<Component, T>())
            return;
        removeComponent(ClassTypeId<Component>::id<T>());
    }

    void removeAllComponents();

    template <typename T>
    T *component() const {
        if (!std::is_base_of<Component, T>())
            return nullptr;
        return static_cast<T*>(component(ClassTypeId<Component>::id<T>()));
    }

    template <typename T>
    bool hasComponent() const {
        if (!std::is_base_of<Component, T>())
            return false;
        return hasComponent(ClassTypeId<Component>::id<T>());
    }

    std::vector<Component *> components() const;
    ComponentMask componentTypeList() const;

    bool operator==(const Entity &entity) const;
    bool operator!=(const Entity &entity) const { return !operator==(entity); }

private:
    void addComponent(Component *component, TypeId componentTypeId);
    void removeComponent(TypeId componentTypeId);
    Component *component(TypeId componentTypeId) const;
    bool hasComponent(TypeId componentTypeId) const;

private:
    World *m_world;
    Id m_id;
};

}

#endif // SGEN_ENTITY_H
