#ifndef SGEN_RESOURCECACHE_H
#define SGEN_RESOURCECACHE_H

#include <sgen/common/export.h>
#include <sgen/core/resource.h>
#include <sgen/core/filter.h>
#include <sgen/common/log.h>

#include <nlohmann/json.hpp>

#include <memory>
#include <unordered_map>
#include <vector>
#include <algorithm>

namespace sgen {

class ResourceTypeList
{
public:
    ResourceTypeList() = default;
    ~ResourceTypeList() = default;

    std::vector<ResourceType> list() const { return m_list; }
    bool check(const ResourceType &type) const {
        return std::find(m_list.begin(), m_list.end(), type) != m_list.end();
    }

    template <class... Args>
    struct Types { };

    template <typename Head, typename... Args>
    static ResourceTypeList make() {
        ResourceTypeList typeList;
        makeHelper(Types<Head, Args...>(), typeList.m_list);
        auto last = std::unique(typeList.m_list.begin(), typeList.m_list.end());
        typeList.m_list.erase(last, typeList.m_list.end());
        return typeList;
    }

private:
    template <typename Head, typename... Args>
    static void makeHelper(Types<Head, Args...> types, std::vector<ResourceType> &list) {
        (void)(types);
        if (!std::is_base_of<Resource, Head>()) {
            list.clear();
            return;
        }
        list.push_back(Head::resourceType());
        makeHelper(Types<Args...>(), list);
    }

    template <typename... Args>
    static void makeHelper(Types<Args...> types, std::vector<ResourceType> &list) {
        (void)(types);
        (void)(list);
    }

private:
    std::vector<ResourceType> m_list;

};

class SGEN_EXPORT IResourceCache
{
public:
    IResourceCache();
    IResourceCache(const ResourceTypeList &resourceTypes);
    virtual ~IResourceCache() = default;

    ResourceTypeList resourceTypes() const;

    virtual bool load(const std::string &filePath);
    virtual bool load(const nlohmann::json &json) { (void)(json); return false; }

    template<typename T>
    bool add(const std::shared_ptr<T> &resource) {
        if (!std::is_base_of<Resource, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a resource");
            return false;
        }

        if (!resource) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource is null!");
            return false;
        }

        ResourceId id = resource->resourceId();
        if (id == INVALID_RESOURCE_ID) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource id is wrong!");
            return false;
        }

        ResourceType type = T::resourceType();

        if (!m_resourceTypes.check(type)) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource type <", type, "> is wrong!");
            return false;
        }

        auto it = m_resources.find(id);
        if (it != m_resources.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource <", id, "> already exist!");
            return false;
        }

        m_resources.emplace(id, resource);
        LINFO("Add resource <", id, "> with type <", type, ">");

        return true;
    }

    bool remove(ResourceId id);

    template<typename T>
    std::shared_ptr<T> get(ResourceId id) const {
        if (!std::is_base_of<Resource, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a resource");
            return nullptr;
        }
        return std::static_pointer_cast<T>(get(id));
    }

    std::shared_ptr<Resource> get(ResourceId id) const;
    bool exist(ResourceId id) const;

private:
    std::unordered_map<ResourceId, std::shared_ptr<Resource>> m_resources;
    ResourceTypeList m_resourceTypes;

};

template <typename... Args>
class ResourceCache : public IResourceCache
{
public:
    ResourceCache() : IResourceCache{ResourceTypeList::make<Args...>()} {}
    ~ResourceCache() = default;
};

}

#endif // SGEN_RESOURCECACHE_H
