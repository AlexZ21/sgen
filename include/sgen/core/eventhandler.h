#ifndef SGEN_EVENTHANDLER_H
#define SGEN_EVENTHANDLER_H

#include <sgen/common/export.h>
#include <sgen/core/idpool.h>
#include <sgen/core/event.h>

#include <vector>

namespace sgen {

class SGEN_EXPORT EventHandler
{
public:
    EventHandler(const std::vector<int32_t> &eventFilter, int32_t eventPriority = 0);
    virtual ~EventHandler();

    virtual bool event(const Event *event);

    int32_t eventPriority() const;
    std::vector<int32_t> eventFilter() const;
    bool filterEvent(int32_t type) const;

    bool ignoreEvent() const;
    void setIgnoreEvent(bool ignore);

private:
    int32_t m_eventPriority;
    std::vector<int32_t> m_eventFilter;
    bool m_ignoreEvent;

};

}

#endif // SGEN_EVENTHANDLER_H
