#ifndef SGEN_EVENMANAGER_H
#define SGEN_EVENMANAGER_H

#include <sgen/common/export.h>
#include <sgen/core/event.h>
#include <sgen/core/eventhandler.h>
#include <sgen/core/idpool.h>

#include <queue>
#include <memory>
#include <unordered_map>
#include <vector>

namespace sgen {

class SGEN_EXPORT EventManager
{
    friend class EventHandler;
public:
    EventManager();
    ~EventManager() = default;

    void sendEvent(Event *event);
    void sendEvent(EventHandler *receiver, Event *event);

    void processEvents();

private:
    void addEventHandler(EventHandler *eventHandler);
    void removeEventHandler(EventHandler *eventHandler);

    void addEventHandlers();
    void removeEventHandlers();

private:
    struct EventInfo {
        EventHandler *receiver = nullptr;
        std::unique_ptr<Event> event = nullptr;
    };

    std::queue<EventInfo> m_events;
    std::vector<EventHandler*> m_addEventHandlers;
    std::vector<EventHandler*> m_removeEventHandlers;
    bool m_needUpdateEventHandlers;
    std::unordered_map<int32_t, std::vector<EventHandler *>> m_eventHandlers;

};

}

#endif // SGEN_EVENMANAGER_H
