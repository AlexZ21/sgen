#ifndef SGEN_SCHEDULER_H
#define SGEN_SCHEDULER_H

#include <sgen/common/export.h>
#include <sgen/core/idpool.h>

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

#include <stdint.h>
#include <functional>
#include <vector>

namespace sgen {

class SGEN_EXPORT Scheduler
{
public:
    Scheduler();
    ~Scheduler() = default;

    Id createTimer(sf::Time interval, bool singleshot, const std::function<void()> &callback);
    bool removeTimer(Id timerId);
    sf::Time timerInerval(Id timerId) const;
    bool setTimerInterval(Id timerId, sf::Time interval);
    bool isTimerSingleShot(Id timerId) const;
    bool setTimerSingleShot(Id timerId, bool singleshot);
    bool setTimerCallback(Id timerId, const std::function<void()> &callback);
    bool startTimer(Id timerId);
    bool stopTimer(Id timerId);
    bool isTimerActive(Id timerId);

    void update(sf::Time elapsed);

private:
    struct Item {
        enum State {
            INACTIVE,
            ACTIVE,
            WILL_BE_REMOVED
        };

        sf::Time start = sf::milliseconds(0);
        sf::Time interval = sf::milliseconds(0);
        bool singleshot = true;
        std::function<void()> callback = {};
        State state = INACTIVE;
    };

    IdObjectPool<Item> m_items;
    sf::Clock m_clock;
    std::vector<Id> m_ids;


};

}

#endif // SGEN_SCHEDULER_H
