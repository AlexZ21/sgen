#ifndef SGEN_SCENE_H
#define SGEN_SCENE_H

#include <sgen/core/world.h>
#include <sgen/common/export.h>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

//#include <

namespace sgen {

class SceneSystem;

class SGEN_EXPORT Scene : public World
{
public:
    Scene();
    ~Scene() = default;

    //! Update and render
    virtual void update(sf::Time elapsed);
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed);

private:
    SceneSystem *m_sceneSystem;

};

}

#endif // SGEN_SCENE_H
