#ifndef SGEN_RESOURCEMANAGER_H
#define SGEN_RESOURCEMANAGER_H

#include <sgen/common/export.h>
#include <sgen/core/resourcecache.h>
#include <sgen/core/resource.h>
#include <sgen/core/classtypeid.h>
#include <sgen/common/log.h>

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

namespace sgen {

class SGEN_EXPORT ResourceManager
{
public:
    ResourceManager() {}
    ~ResourceManager() = default;

    /// Resource cache
    template<typename T>
    bool addCache() {
        if (!std::is_base_of<IResourceCache, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a cache");
            return false;
        }

        TypeId cacheTypeId = ClassTypeId<IResourceCache>::id<T>();
        if (cacheTypeId < m_cache.size() && m_cache[cacheTypeId]) {
            LWARN(LOG_CLASS_METHOD,
                  ": Cache already exist!");
            return false;
        }

        std::unique_ptr<T> cache = std::make_unique<T>();

        std::vector<ResourceType> resourceTypes = cache->resourceTypes().list();
        if (resourceTypes.empty()) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource types are not defined for the cache!");
            return false;
        }

        for (ResourceType &resourceType : resourceTypes) {
            m_resourceTypes.emplace(resourceType, cacheTypeId);
        }

        if (cacheTypeId >= m_cache.size())
            m_cache.resize(cacheTypeId + 1);

        m_cache[cacheTypeId] = std::move(cache);

        return true;
    }

    template<typename T>
    bool removeCache() {
        if (!std::is_base_of<IResourceCache, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a cache");
            return false;
        }

        TypeId cacheTypeId = ClassTypeId<IResourceCache>::id<T>();
        if (cacheTypeId >= m_cache.size() || !m_cache[cacheTypeId]) {
            LWARN(LOG_CLASS_METHOD,
                  ": Cache not found!");
            return false;
        }
        std::vector<ResourceType> resourceTypes = m_cache[cacheTypeId]->resourceTypes().list();
        for (ResourceType &resourceType : resourceTypes) {
            m_resourceTypes.erase(resourceType);
        }
        m_cache[cacheTypeId].reset();
        return true;
    }

    template<typename T>
    T *cache() const {
        if (!std::is_base_of<IResourceCache, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a cache");
            return nullptr;
        }

        TypeId cacheTypeId = ClassTypeId<IResourceCache>::id<T>();
        if (cacheTypeId >= m_cache.size() || !m_cache[cacheTypeId]) {
            LWARN(LOG_CLASS_METHOD,
                  ": Cache not found!");
            return nullptr;
        }

        return static_cast<T>(m_cache[cacheTypeId].get());
    }

    template<typename T>
    T *cache(const ResourceType &resourceType) const {
        return static_cast<T>(cache(resourceType));
    }

    IResourceCache *cache(const ResourceType &resourceType) const {
        auto it = m_resourceTypes.find(resourceType);
        if (it == m_resourceTypes.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource type <", resourceType, "> not found!");
            return nullptr;
        }
        return m_cache[it->second].get();
    }

    void load(const std::string &filePath);

    template<typename T>
    std::shared_ptr<T> get(const std::string &resourceId) const {
        if (!std::is_base_of<Resource, T>()) {
            LWARN(LOG_CLASS_METHOD,
                  ": It is not a resource");
            return nullptr;
        }
        return std::static_pointer_cast<T>(get(T::resourceType(), resourceId));
    }

    std::shared_ptr<Resource> get(const ResourceType &resourceType, const std::string &resourceId) const {
        auto it = m_resourceTypes.find(resourceType);
        if (it == m_resourceTypes.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": Resource type <", resourceType, "> not found!");
            return nullptr;
        }
        return m_cache.at(it->second)->get(resourceId);
    }

private:
    std::vector<std::unique_ptr<IResourceCache>> m_cache;
    std::unordered_map<ResourceType, TypeId> m_resourceTypes;

};

}

#endif // SGEN_RESOURCEMANAGER_H
