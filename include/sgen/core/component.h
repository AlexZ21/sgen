#ifndef SGEN_COMPONENT_H
#define SGEN_COMPONENT_H

#include <sgen/common/export.h>
#include <sgen/core/classtypeid.h>

#include <type_traits>
#include <vector>
#include <bitset>

#define SGEN_COMPONENT_TYPE_ID(_class_) \
    TypeId typeId() { \
    return ClassTypeId<Component>::id<_class_>(); \
    }

namespace sgen {

class SGEN_EXPORT Component
{
public:
    Component() = default;
    virtual ~Component() = default;

    virtual TypeId typeId() = 0;

};

const int32_t MAX_AMOUNT_OF_COMPONENT_MASK = 32;
const int32_t MAX_AMOUNT_OF_COMPONENTS = 32;
using ComponentMask = std::bitset<MAX_AMOUNT_OF_COMPONENT_MASK>;

}

#endif // SGEN_COMPONENT_H
