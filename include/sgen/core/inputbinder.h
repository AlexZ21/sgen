#ifndef SGEN_INPUTBINDER_H
#define SGEN_INPUTBINDER_H

#include <sgen/common/export.h>
#include <sgen/core/eventhandler.h>

#include <string>
#include <unordered_map>
#include <functional>
#include <mutex>

#include <SFML/Window/Event.hpp>

namespace sgen {

class SGEN_EXPORT InputBinder : public EventHandler
{
public:
    enum InputType {
        Hold,
        PressOnce,
        ReleaseOnce
    };

    enum InputDeviceType {
        Keyboard,
        Mouse,
        Joystick
    };

    enum ModifierKeys {
        NoModifier = 0,
        Alt = 1,
        Control = 2,
        Shift = 4,
        System = 8
    };

    struct ShortcutState {
        std::string actionName;
        InputType inputType;
        bool activated = false;
    };

    InputBinder();
    ~InputBinder() = default;

    //! Add action callback
    void addAction(const std::string &name, const std::function<void()> &callback);
    //! Remove action callback by name
    void removeAction(const std::string &name);
    //! Clear all actioncallbacks
    void clearActions();

    //! Bind shortcut to action
    void bind(const std::string &actionName, InputType inputType,
              sf::Keyboard::Key code, ModifierKeys modifiers = NoModifier);
    void bind(const std::string &actionName, InputType inputType,
              sf::Mouse::Button button);
    void bind(const std::string &actionName, InputType inputType,
              uint32_t joystickId, uint32_t button);
    //! Unbind shotcuts by name
    void unbind(const std::string &actionName);
    //! Clear all binds
    void clearBinds();

    bool event(const Event *event);

private:
    std::unordered_multimap<size_t, ShortcutState> m_shortcuts;
    std::unordered_map<std::string, std::function<void()>> m_actions;

};

}

#endif // SGEN_INPUTBINDER_H
