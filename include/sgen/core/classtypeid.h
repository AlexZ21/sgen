#ifndef SGEN_CLASSTYPEID_H
#define SGEN_CLASSTYPEID_H

#include <sgen/common/export.h>

#include <atomic>
#include <string>

namespace sgen {

using TypeId = std::size_t;

template <typename T>
class SGEN_EXPORT ClassTypeId
{
public:
    template <typename U>
    static TypeId id() {
        static const TypeId id = m_next++;
        return id;
    }

private:
    static std::atomic<TypeId> m_next;

};

template <typename T>
std::atomic<TypeId> ClassTypeId<T>::m_next{0};

}

#endif // SGEN_CLASSTYPEID_H
