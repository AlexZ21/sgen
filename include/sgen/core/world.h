﻿#ifndef SGEN_WORLD_H
#define SGEN_WORLD_H

#include <sgen/common/export.h>
#include <sgen/core/entity.h>
#include <sgen/core/system.h>
#include <sgen/core/classtypeid.h>
#include <sgen/common/log.h>

#include <unordered_map>
#include <algorithm>

namespace sgen {

class SGEN_EXPORT World
{
    friend class Entity;
public:
    World();
    World(const World &) = delete;
    World(World &&) = delete;
    World &operator=(const World &) = delete;
    World &operator=(World &&) = delete;
    virtual ~World() = default;

    //! Systems
    template <typename T, typename... Args>
    bool addSystem(Args&&... args) {
        if (!std::is_base_of<System, T>())
            return false;

        TypeId typeId = ClassTypeId<System>::id<T>();
        auto it = std::find_if(m_systems.begin(), m_systems.end(),
                               [typeId](auto &system) { return system->typeId() == typeId; });

        if (it != m_systems.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": System already added to world!");
            return false;
        }

        T *system = new T{std::forward<Args>(args)...};
        m_systems.push_back(std::unique_ptr<T>(system));

        system->setWorld(this);

        return true;
    }

    template <typename T>
    bool removeSystem() {
        if (!std::is_base_of<System, T>())
            return false;

        TypeId typeId = ClassTypeId<System>::id<T>();
        auto it = std::find_if(m_systems.begin(), m_systems.end(),
                               [typeId](auto &system) { return system->typeId() == typeId; });

        if (it == m_systems.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": System not found!");
            return false;
        }

        m_systems.erase(it);

        return true;
    }

    template <typename T>
    bool systemExists() {
        if (!std::is_base_of<System, T>())
            return false;

        TypeId typeId = ClassTypeId<System>::id<T>();
        auto it = std::find_if(m_systems.begin(), m_systems.end(),
                               [typeId](auto &system) { return system->typeId() == typeId; });

        return it != m_systems.end();
    }

    void removeAllSystems();

    template <typename T>
    T *system() {
        if (!std::is_base_of<System, T>())
            return nullptr;

        TypeId typeId = ClassTypeId<System>::id<T>();
        auto it = std::find_if(m_systems.begin(), m_systems.end(),
                               [typeId](auto &system) { return system->typeId() == typeId; });

        if (it == m_systems.end()) {
            LWARN(LOG_CLASS_METHOD,
                  ": System not found!");
            return nullptr;
        }

        return static_cast<T *>(it->get());
    }

    const std::vector<std::unique_ptr<System>> &systems() const;

    //! Entities functions
    Entity createEntity();
    std::vector<Entity> createEntities(size_t amount);
    Entity entity(size_t index);
    const std::vector<Entity> &entities() const;

    void refresh();
    void clear();

private:
    //! Entites functions
    void destroyEntity(const Entity &entity);
    void destroyEntities(const std::vector<Entity> &entities);
    void enableEntity(const Entity &entity);
    void disableEntity(const Entity &entity);
    bool isEnabled(const Entity &entity) const;
    bool isValid(const Entity &entity) const;

    //! Components functions
    bool addComponent(Entity &entity, Component *component, TypeId componentTypeId);
    bool removeComponent(Entity &entity, TypeId componentTypeId);
    void removeAllComponents(Entity &entity);
    Component *component(const Entity &entity, TypeId componentTypeId) const;
    ComponentMask componentMask(const Entity &entity) const;
    std::vector<Component *> components(const Entity &entity) const;
    bool hasComponent(const Entity &entity, TypeId componentTypeId) const;

    void clearEntitiesCache();
    void clearTempEntitiesCache();

private:
    struct EntityState {
        EntityState() : enabled(false), components(MAX_AMOUNT_OF_COMPONENTS) {}
        EntityState(const EntityState &) = delete;
        EntityState(EntityState &&es) {
            enabled = es.enabled;
            systems = std::move(es.systems);
            components = std::move(es.components);
            mask = std::move(es.mask);
        }
        EntityState &operator=(const EntityState &) = delete;
        EntityState &operator=(EntityState &&) = delete;
        ~EntityState() = default;

        bool enabled;
        std::vector<bool> systems;

        std::vector<std::unique_ptr<Component>> components;
        ComponentMask mask;
    };

    std::vector<std::unique_ptr<System>> m_systems;
    IdObjectPool<EntityState> m_entityStates;
    std::vector<Entity> m_aliveEntities;
    std::vector<Entity> m_killedEntities;
    std::vector<Entity> m_enabledEntities;
    std::vector<Entity> m_disabledEntities;

    int32_t m_bufferSize;

};

}

#endif // SGEN_WORLD_H
