#ifndef SGEN_TIMER_H
#define SGEN_TIMER_H

#include <sgen/common/export.h>
#include <sgen/core/idpool.h>
#include <sgen/core/sigslot.h>

#include <SFML/System/Time.hpp>

#include <memory>

namespace sgen {

class Scheduler;

class SGEN_EXPORT Timer
{
    friend class Scheduler;
public:
    Timer();
    ~Timer();

    bool start();
    bool stop();

    bool isActive() const;

    sf::Time interval() const;
    void setInterval(const sf::Time &interval);

    bool singleShot() const;
    void setSingleShot(bool singleShot);

    Signal<> timeout;

private:
    void timeoutCallback();

private:
    std::shared_ptr<Id> m_id;

};

}

#endif // SGEN_TIMER_H
