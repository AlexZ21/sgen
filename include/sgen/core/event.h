#ifndef SGEN_EVENT_H
#define SGEN_EVENT_H

#include <sgen/common/export.h>

#include <SFML/Window/Event.hpp>

#include <stdint.h>

namespace sgen {

// Default event types
namespace EventTypes {

const int32_t Closed = 0;
const int32_t Resized = 1;
const int32_t LostFocus = 2;
const int32_t GainedFocus = 3;
const int32_t TextEntered = 4;
const int32_t KeyPressed = 5;
const int32_t KeyReleased = 6;
const int32_t MouseWheelMoved = 7;
const int32_t MouseWheelScrolled = 8;
const int32_t MouseButtonPressed = 9;
const int32_t MouseButtonReleased = 10;
const int32_t MouseMoved = 11;
const int32_t MouseEntered = 12;
const int32_t MouseLeft = 13;
const int32_t JoystickButtonPressed = 14;
const int32_t JoystickButtonReleased = 15;
const int32_t JoystickMoved = 16;
const int32_t JoystickConnected = 17;
const int32_t JoystickDisconnected = 18;
const int32_t TouchBegan = 19;
const int32_t TouchMoved = 20;
const int32_t TouchEnded = 21;
const int32_t SensorChanged = 22;

}

struct SGEN_EXPORT Event
{
    Event() : type(-1) {}
    ~Event() = default;
    int32_t type;

};

//! SFML events
struct SGEN_EXPORT ClosedEvent : public Event
{
    ClosedEvent() { type = EventTypes::Closed; }
};

struct SGEN_EXPORT ResizedEvent : public Event
{
    ResizedEvent() { type = EventTypes::Resized; }
    uint32_t width = 0;
    uint32_t height = 0;
};

struct SGEN_EXPORT LostFocusEvent : public Event
{
    LostFocusEvent() { type = EventTypes::LostFocus; }
};

struct SGEN_EXPORT GainedFocusEvent : public Event
{
    GainedFocusEvent() { type = EventTypes::GainedFocus; }
};

struct SGEN_EXPORT TextEnteredEvent : public Event
{
    TextEnteredEvent() { type = EventTypes::TextEntered; }
    uint32_t unicode = 0;
};

struct SGEN_EXPORT KeyEvent : public Event
{
    KeyEvent() : Event() {}
    sf::Keyboard::Key code = sf::Keyboard::Unknown;
    bool alt = false;
    bool control = false;
    bool shift = false;
    bool system = false;
};

struct SGEN_EXPORT KeyPressedEvent : public KeyEvent
{
    KeyPressedEvent() { type = EventTypes::KeyPressed; }
};

struct SGEN_EXPORT KeyReleasedEvent : public KeyEvent
{
    KeyReleasedEvent() { type = EventTypes::KeyReleased; }
};

struct SGEN_EXPORT MouseWheelMovedEvent : public Event
{
    MouseWheelMovedEvent() { type = EventTypes::MouseWheelMoved; }
    int32_t delta = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT MouseWheelScrolledEvent : public Event
{
    MouseWheelScrolledEvent() { type = EventTypes::MouseWheelScrolled; }
    sf::Mouse::Wheel wheel = sf::Mouse::VerticalWheel;
    float delta = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT MouseButtonEvent : public Event
{
    MouseButtonEvent() : Event() {}
    sf::Mouse::Button button = sf::Mouse::ButtonCount;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT MouseButtonPressedEvent : public MouseButtonEvent
{
    MouseButtonPressedEvent() { type = EventTypes::MouseButtonPressed; }
};

struct SGEN_EXPORT MouseButtonReleasedEvent : public MouseButtonEvent
{
    MouseButtonReleasedEvent() { type = EventTypes::MouseButtonReleased; }
};

struct SGEN_EXPORT MouseMovedEvent : public Event
{
    MouseMovedEvent() { type = EventTypes::MouseMoved; }
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT MouseEnteredEvent : public Event
{
    MouseEnteredEvent() { type = EventTypes::MouseEntered; }
};

struct SGEN_EXPORT MouseLeftEvent : public Event
{
    MouseLeftEvent() { type = EventTypes::MouseLeft; }
};

struct SGEN_EXPORT JoystickButtonEvent : public Event
{
    JoystickButtonEvent() : Event() {}
    uint32_t joystickId = 0;
    uint32_t button = 0;
};

struct SGEN_EXPORT JoystickButtonPressedEvent : public JoystickButtonEvent
{
    JoystickButtonPressedEvent() { type = EventTypes::JoystickButtonPressed; }
};

struct SGEN_EXPORT JoystickButtonReleasedEvent : public JoystickButtonEvent
{
    JoystickButtonReleasedEvent() { type = EventTypes::JoystickButtonReleased; }
};

struct SGEN_EXPORT JoystickMovedEvent : public Event
{
    JoystickMovedEvent() { type = EventTypes::JoystickMoved; }
    uint32_t joystickId = 0;
    sf::Joystick::Axis axis = sf::Joystick::X;
    float position = 0;
};

struct SGEN_EXPORT JoystickConnectedEvent : public Event
{
    JoystickConnectedEvent() { type = EventTypes::JoystickConnected; }
    uint32_t joystickId = 0;
};

struct SGEN_EXPORT JoystickDisconnectedEvent : public Event
{
    JoystickDisconnectedEvent() { type = EventTypes::JoystickDisconnected; }
    uint32_t joystickId = 0;
};

struct SGEN_EXPORT TouchBeganEvent : public Event
{
    TouchBeganEvent() { type = EventTypes::TouchBegan; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT TouchMovedEvent : public Event
{
    TouchMovedEvent() { type = EventTypes::TouchMoved; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT TouchEndedEvent : public Event
{
    TouchEndedEvent() { type = EventTypes::TouchEnded; }
    uint32_t finger = 0;
    int32_t x = 0;
    int32_t y = 0;
};

struct SGEN_EXPORT SensorChangedEvent : public Event
{
    SensorChangedEvent() { type = EventTypes::SensorChanged; }
    sf::Sensor::Type sensorType = sf::Sensor::Count;
    float x = 0;
    float y = 0;
    float z = 0;
};

SGEN_EXPORT Event *createSfmlEvent(const sf::Event &sfmlEvent);

}

#endif // SGEN_EVENT_H
