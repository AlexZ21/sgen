#ifndef SGEN_GAME_H
#define SGEN_GAME_H

#include <sgen/common/export.h>

#include <SFML/Graphics/RenderWindow.hpp>

#include <string>
#include <memory>
#include <atomic>

namespace sgen {

class EventManager;
class InputBinder;
class ResourceManager;
class Scheduler;
class Scene;
class GuiManager;

class SGEN_EXPORT Game
{
public:
    static Game *instance();

    //!Show window
    bool showWindow(uint32_t width, uint32_t height, std::string title, bool fullscreen = false);
    //! Close window
    bool closeWindow();

    //! Execute main loop
    int exec();
    //! Exit from main loop
    void exit();

    std::string name() const;
    void setName(const std::string &name);

    std::string author() const;
    void setAuthor(const std::string &author);

    std::string version() const;
    void setVersion(const std::string &version);

    EventManager *eventManager() const;

    void setInputBinder(const std::shared_ptr<InputBinder> &inputBinder);
    std::shared_ptr<InputBinder> inputBinder() const;

    ResourceManager *resourceManager() const;
    Scheduler *scheduler() const;

    std::shared_ptr<Scene> scene() const;
    void setScene(const std::shared_ptr<Scene> &scene);

    GuiManager *guiManager() const;

private:
    Game();
    ~Game();

    void init();

    //! Main loop function
    void mainLoop();
    //! Tick function
    void tickFunc(sf::Time elapsed);

private:
    static Game *m_instance;

    std::string m_name;
    std::string m_author;
    std::string m_version;

    sf::Time m_tickTime; ///< Loop tick time: 16 msec
    std::atomic_bool m_running; ///< Game loop state

    uint32_t m_width; ///< Window width
    uint32_t m_height; ///< Window height
    std::string m_title; ///< Window title
    bool m_fullscreen; ///< Open window in fullscreen
    std::shared_ptr<sf::RenderWindow> m_window;

    std::unique_ptr<EventManager> m_eventManager; ///< Event manager
    std::shared_ptr<InputBinder> m_inputBinder; ///< Input event callbacks
    std::unique_ptr<ResourceManager> m_resourceManager;
    std::unique_ptr<Scheduler> m_scheduler;
    std::shared_ptr<Scene> m_scene;
    std::unique_ptr<GuiManager> m_guiManager;

};

}

#endif // SGEN_GAME_H
