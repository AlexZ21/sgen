#ifndef SGEN_SYSTEM_H
#define SGEN_SYSTEM_H

#include <sgen/common/export.h>
#include <sgen/core/entity.h>
#include <sgen/core/component.h>
#include <sgen/core/filter.h>

#define SGEN_SYSTEM_TYPE_ID(_class_) \
    TypeId typeId() { \
    return ClassTypeId<System>::id<_class_>(); \
    }

namespace sgen {

class World;

class System
{
    friend class World;
public:
    System(const Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK> &filter);
    virtual ~System() = default;

    virtual TypeId typeId() = 0;

    World *world() const;
    const Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK> &filter() const;
    const std::vector<Entity> &entities() const;

    virtual void update(int32_t milliseconds) { (void)(milliseconds); }

protected:
    virtual void initialize() {}
    virtual void onEntityAdded(const Entity &entity) { (void)(entity); }
    virtual void onEntityRemoved(const Entity &entity) { (void)(entity); }

private:
    void add(const Entity &entity);
    void remove(const Entity &entity);

    void setWorld(World *world);

private:
    World *m_world;
    Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK> m_filter;
    std::vector<Entity> m_entities;
};

template <typename... Args>
class FilteredSystem : public System
{
public:
    FilteredSystem() : System{Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK>::make<Args...>()} {}
    ~FilteredSystem() = default;
};

}

#endif // SGEN_SYSTEM_H
