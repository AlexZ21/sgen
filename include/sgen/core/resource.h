#ifndef SGEN_RESOURCE_H
#define SGEN_RESOURCE_H

#include <sgen/common/export.h>
#include <sgen/common/consts.h>

#include <stdint.h>

#define SGEN_RESOURCE_TYPE(_ARG_) \
constexpr static ResourceType resourceType() { return _ARG_; }

namespace sgen {

class ResourceManager;

class SGEN_EXPORT Resource
{
    friend class ResourceManager;
public:
    Resource();
    Resource(ResourceId t_id);
    virtual ~Resource() = default;

    ResourceId resourceId() const;
    void setResourceId(ResourceId t_id);

    SGEN_RESOURCE_TYPE(INVALID_RESOURCE_TYPE)

private:
    ResourceId m_id;

};

}

#endif // SGEN_RESOURCE_H
