#ifndef SGEN_WIDGET_H
#define SGEN_WIDGET_H

#include <sgen/common/export.h>
#include <sgen/core/event.h>
#include <sgen/core/sigslot.h>
#include <sgen/graphics/drawable.h>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <vector>
#include <memory>

namespace sgen {

class GuiTheme;
class GuiManager;
class Layout;

class SGEN_EXPORT Widget : public Drawable, public std::enable_shared_from_this<Widget>
{
    friend class GuiManager;
public:
    template<typename T, typename... Args>
    static std::shared_ptr<T> create(const std::shared_ptr<Widget> &parent, Args &&...args) {
        if (!std::is_base_of<Widget, T>())
            return nullptr;

        std::shared_ptr<T> widget = std::make_shared<T>(std::forward<Args>(args)...);
        if (!widget)
            return nullptr;

        if (parent)
            parent->addChild(widget);

        return widget;
    }

    Widget();
    virtual ~Widget();

    virtual bool event(const Event *event);
    void draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const;

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void setPosition(float x, float y);
    void setPosition(const sf::Vector2f &position);

    sf::Vector2i size() const;
    void setSize(const sf::Vector2i &size);
    void setSize(int32_t width, int32_t height);

    sf::Vector2i minimumSize() const;
    void setMinimumSize(const sf::Vector2i &minimumSize);
    void setMinimumSize(int32_t width, int32_t height);

    void setParent(const std::shared_ptr<Widget> &parent);
    void addChild(const std::shared_ptr<Widget> &child);
    void removeChild(const std::shared_ptr<Widget> &child);

    std::shared_ptr<GuiTheme> theme() const;
    void setTheme(const std::shared_ptr<GuiTheme> &theme, bool updateChildren = true);

    std::vector<int32_t> contentMargins() const;
    void setContentMargins(const std::vector<int32_t> &contentMargins);

    bool isEnabled() const;
    void setEnabled(bool enabled);

    bool isVisible() const;
    void setVisible(bool visible);

    bool isMouseHover() const;

    bool isIgnoreMouseHover() const;
    void setIgnoreMouseHover(bool ignoreMouseHover);

    bool isMouseDown() const;

    bool isDraggable() const;
    void setDraggable(bool draggable);

    sf::Vector2i globalToLocal(const sf::Vector2i &point) const;
    sf::Vector2i globalToLocal(int32_t x, int32_t y) const;

    bool isChild(const std::shared_ptr<Widget> &widget) const;

    std::shared_ptr<Layout> layout() const;
    void setLayout(const std::shared_ptr<Layout> &layout);

    virtual bool dropWidget(const sf::Vector2i &point, const sf::Vector2i &handlePoint, const std::shared_ptr<Widget> &widget);

    void updateGlobalBounds();
    void updateLayoutContentGeometry();

private:
    void setMouseHover(bool mouseHover);
    void setMouseDown(bool mouseDown);

public:
    Signal<int32_t, int32_t> resized;
    Signal<> themeChanged;
    Signal<bool> visibleChanged;
    Signal<bool> enableChanged;
    Signal<bool> mouseHoverChanged;
    Signal<bool> mouseDownChanged;

private:
    void drawWidget(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const;

private:
    std::weak_ptr<Widget> m_parent;
    std::vector<std::shared_ptr<Widget>> m_children;

    std::shared_ptr<GuiTheme> m_theme;

    sf::Vector2i m_size;
    sf::Vector2i m_minimumSize;

    mutable sf::FloatRect m_globalBounds;
    mutable bool m_needUpdateGlobalBounds;

    bool m_enabled;
    bool m_visible;

    bool m_mouseHover;
    bool m_ignoreMouseHover;
    bool m_mouseDown;

    bool m_focused;
    bool m_focusable;

    bool m_draggable;

    std::vector<int32_t> m_contentMargins;

    std::shared_ptr<Layout> m_layout;


};

}

#endif // SGEN_WIDGET_H
