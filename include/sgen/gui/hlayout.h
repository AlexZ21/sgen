#ifndef SGEN_HLAYOUT_H
#define SGEN_HLAYOUT_H

#include <sgen/gui/layout.h>

namespace sgen {

class SGEN_EXPORT HLayout : public Layout
{
public:
    HLayout();
    ~HLayout() = default;

protected:
    void updateContentGeometry();
    sf::Vector2i minimumSize();

};

}

#endif // SGEN_HLAYOUT_H
