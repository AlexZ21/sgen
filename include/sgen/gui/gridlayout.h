#ifndef SGEN_GRIDLAYOUT_H
#define SGEN_GRIDLAYOUT_H

#include <sgen/gui/layout.h>

namespace sgen {

class SGEN_EXPORT GridLayout : public Layout
{
public:
    GridLayout();
    ~GridLayout() = default;

protected:
    void updateContentGeometry();


private:
    int32_t m_columns;

};

}

#endif // SGEN_GRIDLAYOUT_H
