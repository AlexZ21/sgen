#ifndef SGEN_FRAMEDWIDGET_H
#define SGEN_FRAMEDWIDGET_H

#include <sgen/common/export.h>
#include <sgen/gui/widget.h>
#include <sgen/gui/guiprimitive.h>
#include <sgen/graphics/texture.h>

#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Transform.hpp>

namespace sgen {

class SGEN_EXPORT FramedWidget : public Widget
{
public:
    FramedWidget();
    ~FramedWidget() = default;

    void draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const;

private:
    FramedPrimitive m_framedPrimitive;

};

}

#endif // SGEN_FRAMEDWIDGET_H
