#ifndef SGEN_GUIPRIMITIVE_H
#define SGEN_GUIPRIMITIVE_H

#include <sgen/common/export.h>
#include <sgen/gui/guitheme.h>
#include <sgen/graphics/texture.h>

#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <string>
#include <vector>

namespace sgen {

class SGEN_EXPORT FramedPrimitive
{
public:
    FramedPrimitive();
    ~FramedPrimitive() = default;

    void setSize(const sf::Vector2i &size);
    void setSize(int32_t width, int32_t height);
    void setTexturePosition(const sf::IntRect &rect, const std::vector<int32_t> &borders);

    void draw(sf::RenderTarget &target, sf::RenderStates state) const;

private:
    void updateVericesPostion();
    void updateTexturePosition();

private:
    sf::VertexArray m_vertices;
    sf::Vector2i m_size;
    std::vector<int32_t> m_borders;
    sf::IntRect m_rect;

};

}

#endif // SGEN_GUIPRIMITIVE_H
