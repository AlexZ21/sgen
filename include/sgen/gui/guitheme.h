#ifndef SGEN_GUITHEME_H
#define SGEN_GUITHEME_H

#include <sgen/common/export.h>
#include <sgen/core/resource.h>
#include <sgen/graphics/texture.h>

#include <unordered_map>
#include <memory>
#include <vector>

#include <SFML/Graphics/Rect.hpp>

namespace sgen {

namespace WidgetStyleTypes {
const int32_t FrameWidget = 1;
}

struct WidgetStyle
{
    int32_t type = -1;
};

struct FrameWidgetStyle : public WidgetStyle
{
    struct State {
        std::shared_ptr<Texture> texture;
        sf::IntRect rect;
        std::vector<int32_t> borders;
    };

    FrameWidgetStyle() { type = WidgetStyleTypes::FrameWidget; }
    std::unordered_map<std::string, State> states;
};

class SGEN_EXPORT GuiTheme : public Resource
{
public:
    GuiTheme();
    ~GuiTheme() = default;

    std::shared_ptr<WidgetStyle> style(int32_t styleType) const;
    void setStyle(const std::shared_ptr<WidgetStyle> &style);

    SGEN_RESOURCE_TYPE(GUI_THEME_RESOURCE_TYPE)

private:
    std::unordered_map<int32_t, std::shared_ptr<WidgetStyle>> m_styles;

};

}

#endif // SGEN_GUITHEME_H
