#ifndef SGEN_GUIMANAGER_H
#define SGEN_GUIMANAGER_H

#include <sgen/common/export.h>
#include <sgen/core/eventhandler.h>

#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

#include <memory>

namespace sgen {

class Widget;
class GuiTheme;

class SGEN_EXPORT GuiManager : public EventHandler
{
    friend class Widget;
public:
    GuiManager();
    ~GuiManager() = default;

    bool event(const Event *event);
    void draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed);

    std::shared_ptr<Widget> rootWidget() const;
    void setRootWidget(const std::shared_ptr<Widget> &rootWidget);

    std::shared_ptr<GuiTheme> theme() const;
    void setTheme(const std::shared_ptr<GuiTheme> &theme, bool updateChildren = true);

    std::shared_ptr<Widget> widgetAtPoint(int32_t x, int32_t y, const std::shared_ptr<Widget> &widget);

private:
    void setRecursiveIgnoreMouseHover(const std::shared_ptr<Widget> &widget, bool ignore);

private:
    std::shared_ptr<Widget> m_rootWidget;
    std::shared_ptr<GuiTheme> m_theme;
    sf::View m_view;
    std::shared_ptr<Widget> m_hoveredWidget;
    std::shared_ptr<Widget> m_downedWidget;
    std::shared_ptr<Widget> m_draggedWidget;
    sf::Vector2i m_draggedPointStart;
    sf::Vector2i m_draggedPointCurrent;

};

}

#endif // SGEN_GUIMANAGER_H
