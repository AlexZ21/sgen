#ifndef SGEN_GUITHEMECACHE_H
#define SGEN_GUITHEMECACHE_H

#include <sgen/common/export.h>
#include <sgen/core/resourcecache.h>
#include <sgen/gui/guitheme.h>
#include <sgen/common/log.h>

namespace sgen {

class SGEN_EXPORT GuiThemeCache : public ResourceCache<GuiTheme>
{
public:
    GuiThemeCache() = default;
    ~GuiThemeCache() = default;

    bool load(const nlohmann::json &json);


};

}

#endif // SGEN_GUITHEMECACHE_H
