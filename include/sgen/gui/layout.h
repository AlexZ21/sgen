#ifndef SGEN_LAYOUT_H
#define SGEN_LAYOUT_H

#include <sgen/common/export.h>

#include <SFML/System/Vector2.hpp>

#include <vector>
#include <memory>

namespace sgen {

class Widget;

class SGEN_EXPORT Layout
{
    friend class Widget;
public:
    struct Item {
        friend class Layout;

        virtual ~Item() = default;

        enum Type {
            WIDGET,
            LAYOUT,
            STRECH,
            SPACING
        };

        Type type;
        int32_t strechFactor;
        sf::Vector2i size;

        virtual void updateContentGeometry(const sf::Vector2i &position, const sf::Vector2i &size) { (void)(position); (void)(size); }
        virtual sf::Vector2i minimumSize() { return {0, 0}; }
    };

    struct WidgetItem : public Item {
        WidgetItem() { type = WIDGET; strechFactor = 1; }
        virtual ~WidgetItem() {}

        std::shared_ptr<Widget> widget;

        void updateContentGeometry(const sf::Vector2i &position, const sf::Vector2i &size);
        sf::Vector2i minimumSize();
    };

    struct LayoutItem : public Item {
        LayoutItem() { type = LAYOUT; strechFactor = 1;}
        virtual ~LayoutItem() = default;

        std::shared_ptr<Layout> layout;

        void updateContentGeometry(const sf::Vector2i &position, const sf::Vector2i &size);
        sf::Vector2i minimumSize();
    };

    struct StrechItem : public Item {
        StrechItem() { type = STRECH; strechFactor = 1;}
        virtual ~StrechItem() = default;
    };

    struct SpacingItem : public Item {
        SpacingItem() { type = SPACING; strechFactor = 1;}
        virtual ~SpacingItem() = default;
        int32_t space = 0;
        sf::Vector2i minimumSize() { return {space, space}; }
    };

    Layout();
    virtual ~Layout() = default;

    void addWidget(const std::shared_ptr<Widget> &widget, int32_t strechFactor = 1);
    void addLayout(const std::shared_ptr<Layout> &layout, int32_t strechFactor = 1);
    void addStrech(int32_t strechFactor = 1);
    void addSpacing(int32_t space);
    void removeItem(int32_t index);

    const std::vector<std::shared_ptr<Item>> &items() const;

    std::shared_ptr<Item> findWidget(const std::shared_ptr<Widget> &widget) const;
    std::shared_ptr<Item> findLayout(const std::shared_ptr<Layout> &layout) const;

    int32_t spacing() const;
    void setSpacing(const int32_t &value);

protected:
    void setPosition(const sf::Vector2i &position);
    void setSize(const sf::Vector2i &size);
    virtual void updateContentGeometry() {}
    virtual sf::Vector2i minimumSize() { return {0, 0}; }

protected:
    std::vector<std::shared_ptr<Item>> p_items;
    int32_t p_maxFactor;
    int32_t p_maxSpacing;
    int32_t p_spacing;
    sf::Vector2i p_position;
    sf::Vector2i p_size;

};

}

#endif // SGEN_LAYOUT_H
