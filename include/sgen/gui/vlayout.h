#ifndef SGEN_VLAYOUT_H
#define SGEN_VLAYOUT_H

#include <sgen/gui/layout.h>

namespace sgen {

class SGEN_EXPORT VLayout : public Layout
{
public:
    VLayout();
    ~VLayout() = default;

protected:
    void updateContentGeometry();
    sf::Vector2i minimumSize();

};

}

#endif // SGEN_VLAYOUT_H
