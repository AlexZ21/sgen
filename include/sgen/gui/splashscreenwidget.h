#ifndef SGEN_SPLASHSCREENWIDGET_H
#define SGEN_SPLASHSCREENWIDGET_H

#include <sgen/common/export.h>
#include <sgen/gui/widget.h>

namespace sgen {

class SplashScreenWidget : public Widget
{
public:
    SplashScreenWidget();
    ~SplashScreenWidget() = default;

    void draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const;

};

}

#endif // SGEN_SPLASHSCREENWIDGET_H
