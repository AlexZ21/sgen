#ifndef SGEN_AABBTREE_H
#define SGEN_AABBTREE_H

#include <sgen/common/export.h>
#include <sgen/core/idpool.h>
#include <sgen/graphics/aabb.h>
#include <sgen/common/log.h>

#include <stdint.h>
#include <stack>
#include <vector>
#include <functional>

namespace sgen {

const float AABB_TREE_EXTENSION = 0.1f;

template<typename T>
class SGEN_EXPORT AABBTree
{
public:
    struct Node {
        bool isLeaf() const {
            return child1 == INVALID_ID;
        }

        AABB aabb;

        Id parent = INVALID_ID;
        Id child1 = INVALID_ID;
        Id child2 = INVALID_ID;

        int32_t height = -1;

        T userData;
    };

    AABBTree() : m_root(INVALID_ID) {}
    ~AABBTree() = default;

    Id insertObject(const AABB &aabb, T userData) {
        Id id = m_nodes.create();

        Node *node = m_nodes[id];
        node->aabb = aabb;
        node->aabb.fatten(AABB_TREE_EXTENSION);
        node->parent = INVALID_ID;
        node->child1 = INVALID_ID;
        node->child2 = INVALID_ID;
        node->height = 0;
        node->userData = userData;

        insertLeaf(id);

        return id;
    }

    void removeObject(const Id &id) {
        if (!m_nodes.valid(id))
            return;
        removeLeaf(id);
        m_nodes.remove(id);
    }

    void updateObject(const Id &id, const AABB &aabb) {
        if (!m_nodes.valid(id))
            return;

        if (m_nodes[id]->aabb.contains(aabb))
            return;

        removeLeaf(id);

        Node *node = m_nodes[id];
        node->aabb = aabb;
        node->aabb.fatten(AABB_TREE_EXTENSION);

        insertLeaf(id);
    }

    std::vector<T> queryOverlaps(const AABB &aabb) const {
        std::vector<T> overlaps;
        std::stack<Id> ids;

        ids.push(m_root);
        while(!ids.empty()) {
            Id id = ids.top();
            ids.pop();

            if (!m_nodes.valid(id))
                continue;

            const Node *node = m_nodes.at(id);

            if (node->aabb.overlaps(aabb)) {
                if (node->isLeaf()) {
                    overlaps.push_back(node->userData);
                } else {
                    ids.push(node->child1);
                    ids.push(node->child2);
                }
            }
        }

        return overlaps;
    }

    void queryOverlaps(const AABB &aabb, const std::function<void (const T&)> &func) const {
        if (!func)
            return;

        std::stack<Id> ids;

        ids.push(m_root);
        while(!ids.empty()) {
            Id id = ids.top();
            ids.pop();

            if (!m_nodes.valid(id))
                continue;

            const Node *node = m_nodes.at(id);

            if (node->aabb.overlaps(aabb)) {
                if (node->isLeaf()) {
                    func(node->userData);
                } else {
                    ids.push(node->child1);
                    ids.push(node->child2);
                }
            }
        }
    }

private:
    void insertLeaf(const Id &leaf) {
        if (!m_root) {
            m_root = leaf;
            m_nodes[m_root]->parent = INVALID_ID;
            return;
        }

        AABB leafAABB = m_nodes[leaf]->aabb;
        Id id = m_root;
        while (!m_nodes[id]->isLeaf()) {
            Id child1 = m_nodes[id]->child1;
            Id child2 = m_nodes[id]->child2;

            float area = m_nodes[id]->aabb.area();

            AABB combinedAABB;
            combinedAABB.merge(m_nodes[id]->aabb, leafAABB);
            float combinedArea = combinedAABB.area();

            // Cost of creating a new parent for this node and the new leaf
            float cost = 2.0f * combinedArea;
            // Minimum cost of pushing the leaf further down the tree
            float inheritanceCost = 2.0f * (combinedArea - area);

            // Cost of descending into child1
            float cost1 = 0.0f;
            if (m_nodes[child1]->isLeaf()) {
                AABB aabb;
                aabb.merge(leafAABB, m_nodes[child1]->aabb);
                cost1 = aabb.area() + inheritanceCost;
            } else {
                AABB aabb;
                aabb.merge(leafAABB, m_nodes[child1]->aabb);
                float oldArea = m_nodes[child1]->aabb.area();
                float newArea = aabb.area();
                cost1 = (newArea - oldArea) + inheritanceCost;
            }

            // Cost of descending into child2
            float cost2 = 0.0f;
            if (m_nodes[child2]->isLeaf()) {
                AABB aabb;
                aabb.merge(leafAABB, m_nodes[child2]->aabb);
                cost2 = aabb.area() + inheritanceCost;
            } else {
                AABB aabb;
                aabb.merge(leafAABB, m_nodes[child2]->aabb);
                float oldArea = m_nodes[child2]->aabb.area();
                float newArea = aabb.area();
                cost2 = newArea - oldArea + inheritanceCost;
            }

            // Descend according to the minimum cost.
            if (cost < cost1 && cost < cost2)
                break;

            // Descend
            id = cost1 < cost2 ? child1 : child2;
        }

        Id sibling = id;

        // Create a new parent.
        Id oldParent = m_nodes[sibling]->parent;
        Id newParent = m_nodes.create();
        m_nodes[newParent]->parent = oldParent;
        m_nodes[newParent]->userData = T();
        m_nodes[newParent]->aabb.merge(leafAABB, m_nodes[sibling]->aabb);
        m_nodes[newParent]->height = m_nodes[sibling]->height + 1;

        if (oldParent) {
            // The sibling was not the root.
            if (m_nodes[oldParent]->child1 == sibling) {
                m_nodes[oldParent]->child1 = newParent;
            } else {
                m_nodes[oldParent]->child2 = newParent;
            }

            m_nodes[newParent]->child1 = sibling;
            m_nodes[newParent]->child2 = leaf;
            m_nodes[sibling]->parent = newParent;
            m_nodes[leaf]->parent = newParent;
        } else {
            // The sibling was the root.
            m_nodes[newParent]->child1 = sibling;
            m_nodes[newParent]->child2 = leaf;
            m_nodes[sibling]->parent = newParent;
            m_nodes[leaf]->parent = newParent;
            m_root = newParent;
        }

        // Walk back up the tree fixing heights and AABBs
        id = m_nodes[leaf]->parent;
        while (id) {
            id = balance(id);

            Id child1 = m_nodes[id]->child1;
            Id child2 = m_nodes[id]->child2;

            m_nodes[id]->height = 1 + std::max(m_nodes[child1]->height, m_nodes[child2]->height);
            m_nodes[id]->aabb.merge(m_nodes[child1]->aabb, m_nodes[child2]->aabb);

            id = m_nodes[id]->parent;
        }
    }

    void removeLeaf(const Id &leaf) {
        if (leaf == m_root){
            m_root = INVALID_ID;
            return;
        }

        Id parent = m_nodes[leaf]->parent;
        Id grandParent = m_nodes[parent]->parent;
        Id sibling;

        if (m_nodes[parent]->child1 == leaf) {
            sibling = m_nodes[parent]->child2;
        } else {
            sibling = m_nodes[parent]->child1;
        }

        if (grandParent) {
            // Destroy parent and connect sibling to grandParent.
            if (m_nodes[grandParent]->child1 == parent) {
                m_nodes[grandParent]->child1 = sibling;
            } else {
                m_nodes[grandParent]->child2 = sibling;
            }
            m_nodes[sibling]->parent = grandParent;
            m_nodes.remove(parent);

            // Adjust ancestor bounds.
            Id id = grandParent;
            while (id) {
                id = balance(id);

                Id child1 = m_nodes[id]->child1;
                Id child2 = m_nodes[id]->child2;

                m_nodes[id]->aabb.merge(m_nodes[child1]->aabb, m_nodes[child2]->aabb);
                m_nodes[id]->height = 1 + std::max(m_nodes[child1]->height, m_nodes[child2]->height);

                id = m_nodes[id]->parent;
            }
        } else {
            m_root = sibling;
            m_nodes[sibling]->parent = INVALID_ID;
            m_nodes.remove(parent);
        }

    }

    Id balance(const Id &iA) {
        Node *A = m_nodes[iA];

        if (A->isLeaf() || A->height < 2) {
            return iA;
        }

        Id iB = A->child1;
        Id iC = A->child2;

        Node *B = m_nodes[iB];
        Node *C = m_nodes[iC];

        int32_t balance = C->height - B->height;

        // Rotate C up
        if (balance > 1) {
            Id iF = C->child1;
            Id iG = C->child2;
            Node *F = m_nodes[iF];
            Node *G = m_nodes[iG];

            // Swap A and C
            C->child1 = iA;
            C->parent = A->parent;
            A->parent = iC;

            // A's old parent should point to C
            if (C->parent) {
                if (m_nodes[C->parent]->child1 == iA) {
                    m_nodes[C->parent]->child1 = iC;
                } else {
                    m_nodes[C->parent]->child2 = iC;
                }
            } else {
                m_root = iC;
            }

            // Rotate
            if (F->height > G->height) {
                C->child2 = iF;
                A->child2 = iG;
                G->parent = iA;
                A->aabb.merge(B->aabb, G->aabb);
                C->aabb.merge(A->aabb, F->aabb);

                A->height = 1 + std::max(B->height, G->height);
                C->height = 1 + std::max(A->height, F->height);
            } else {
                C->child2 = iG;
                A->child2 = iF;
                F->parent = iA;
                A->aabb.merge(B->aabb, F->aabb);
                C->aabb.merge(A->aabb, G->aabb);

                A->height = 1 + std::max(B->height, F->height);
                C->height = 1 + std::max(A->height, G->height);
            }

            return iC;
        }

        // Rotate B up
        if (balance < -1) {
            Id iD = B->child1;
            Id iE = B->child2;
            Node *D = m_nodes[iD];
            Node *E = m_nodes[iE];

            // Swap A and B
            B->child1 = iA;
            B->parent = A->parent;
            A->parent = iB;

            // A's old parent should point to B
            if (B->parent)  {
                if (m_nodes[B->parent]->child1 == iA) {
                    m_nodes[B->parent]->child1 = iB;
                } else {
                    m_nodes[B->parent]->child2 = iB;
                }
            } else {
                m_root = iB;
            }

            // Rotate
            if (D->height > E->height) {
                B->child2 = iD;
                A->child1 = iE;
                E->parent = iA;
                A->aabb.merge(C->aabb, E->aabb);
                B->aabb.merge(A->aabb, D->aabb);

                A->height = 1 + std::max(C->height, E->height);
                B->height = 1 + std::max(A->height, D->height);
            } else {
                B->child2 = iE;
                A->child1 = iD;
                D->parent = iA;
                A->aabb.merge(C->aabb, D->aabb);
                B->aabb.merge(A->aabb, E->aabb);

                A->height = 1 + std::max(C->height, D->height);
                B->height = 1 + std::max(A->height, E->height);
            }

            return iB;
        }

        return iA;
    }


private:
    Id m_root;
    IdObjectPool<Node> m_nodes;

};

}

#endif // SGEN_AABBTREE_H
