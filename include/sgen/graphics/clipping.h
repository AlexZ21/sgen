#ifndef SGEN_CLIPPING_H
#define SGEN_CLIPPING_H

#include <sgen/common/export.h>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/View.hpp>

namespace sgen {

class SGEN_EXPORT Clipping
{
public:
    Clipping(sf::RenderTarget &target, const sf::RenderStates &states, sf::Vector2f topLeft, sf::Vector2f size);
    Clipping(const Clipping& copy) = delete;
    Clipping& operator=(const Clipping& right) = delete;
    ~Clipping();

    static void setOriginalView(const sf::View& view);

private:
    sf::RenderTarget& m_target;
    sf::View m_oldView;

    static sf::View m_originalView;

};

}

#endif // SGEN_CLIPPING_H
