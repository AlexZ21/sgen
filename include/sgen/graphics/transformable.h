#ifndef SGEN_TRANSFORMABLE_H
#define SGEN_TRANSFORMABLE_H

#include <sgen/common/export.h>

#include <SFML/Graphics/Transform.hpp>

namespace sgen {

class SGEN_EXPORT Transformable
{
public:
    Transformable();
    virtual ~Transformable() = default;

    //! Position
    const sf::Vector2f &position() const;
    void setPosition(float x, float y);
    void setPosition(const sf::Vector2f &position);

    //! Roatation
    float rotation() const;
    void setRotation(float angle);

    //! Scale
    const sf::Vector2f &scale() const;
    void setScale(float factorX, float factorY);
    void setScale(const sf::Vector2f &factors);

    //! Origin
    const sf::Vector2f &origin() const;
    void setOrigin(float x, float y);
    void setOrigin(const sf::Vector2f &origin);

    //! Actions
    void move(float offsetX, float offsetY);
    void move(const sf::Vector2f &offset);

    void rotate(float angle);

    void scale(float factorX, float factorY);
    void scale(const sf::Vector2f &factor);

    //! Transform
    const sf::Transform &transform() const;
    const sf::Transform &inverseTransform() const;
    void setTransform(const sf::Transform &transform);

    bool transformNeedUpdate() const;
    bool inverseTransformNeedUpdate() const;

private:
    sf::Vector2f m_position;
    float m_rotation;
    sf::Vector2f m_scale;
    sf::Vector2f m_origin;

    mutable sf::Transform m_transform;
    mutable bool m_transformNeedUpdate;

    mutable sf::Transform m_inverseTransform;
    mutable bool m_inverseTransformNeedUpdate;

};

}

#endif // SGEN_TRANSFORMABLE_H
