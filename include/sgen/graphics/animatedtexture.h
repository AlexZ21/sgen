#ifndef SGEN_ANIMATEDTEXTURE_H
#define SGEN_ANIMATEDTEXTURE_H

#include <sgen/common/export.h>
#include <sgen/graphics/texture.h>

#include <SFML/System/Time.hpp>

#include <vector>
#include <memory>

namespace sgen {

class SGEN_EXPORT AnimatedTexture : public Texture
{
public:
    AnimatedTexture(const std::shared_ptr<sf::Texture> &source,
                    bool looped,
                    const sf::Time &frameTime,
                    const std::vector<sf::IntRect> frames);
    AnimatedTexture(const std::shared_ptr<sf::Texture> &source,
                    bool looped,
                    const sf::Time &frameTime,
                    const sf::Vector2i &frameSize,
                    const sf::Vector2i &frameCounts,
                    const sf::Vector2i &startPoint = sf::Vector2i(0, 0));
    ~AnimatedTexture() = default;

    sf::IntRect frameRect(size_t frame) const;
    size_t frameCount() const;
    sf::Time frameTime() const;
    bool isLooped() const;

    Type type() const;
    std::shared_ptr<sf::Texture> source() const;
    sf::IntRect rect() const;

    SGEN_RESOURCE_TYPE(TEXTURE_RESOURCE_TYPE)

private:
    std::shared_ptr<sf::Texture> m_source;
    std::vector<sf::IntRect> m_frames;
    bool m_looped;
    sf::Time m_frameTime;

};

}

#endif // SGEN_ANIMATEDTEXTURE_H
