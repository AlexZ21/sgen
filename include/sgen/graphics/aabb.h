#ifndef SGEN_AABB_H
#define SGEN_AABB_H

#include <sgen/common/export.h>

#include <SFML/Graphics/Rect.hpp>

namespace sgen {

class SGEN_EXPORT AABB
{
public:
    AABB();
    AABB(float t_minX, float t_minY, float t_maxX, float t_maxY);
    AABB(const AABB &other);
    AABB(const sf::FloatRect &rect);
    ~AABB() = default;

    AABB operator=(const AABB &other);
    AABB operator=(const sf::FloatRect &rect);

    bool operator==(const AABB &other);
    bool operator==(const sf::FloatRect &rect);

    bool isValid() const;

    float width() const;
    float height() const;

    float area() const;

    bool overlaps(const AABB &other) const;
    bool overlaps(const sf::FloatRect &rect) const;

    bool contains(const AABB &other) const;
    bool contains(const sf::FloatRect &rect) const;

    AABB merge(const AABB &other) const;
    AABB merge(const sf::FloatRect &rect) const;
    void merge(const AABB &other1, const AABB &other2);

    AABB intersection(const AABB &other) const;
    AABB intersection(const sf::FloatRect &rect) const;

    void fatten(float extension);

    float minX;
    float minY;
    float maxX;
    float maxY;

};

}

#endif // SGEN_AABB_H
