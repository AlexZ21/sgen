#ifndef SGEN_SPRITE_H
#define SGEN_SPRITE_H

#include <sgen/common/export.h>
#include <sgen/graphics/drawable.h>
#include <sgen/core/resource.h>
#include <sgen/graphics/texture.h>
#include <sgen/graphics/animatedtexturestate.h>

#include <SFML/Graphics/VertexArray.hpp>

#include <memory>

namespace sgen {

class SGEN_EXPORT Sprite : public Drawable
{
public:
    Sprite();
    Sprite(const std::shared_ptr<Texture> &texture, bool updateSize = true);
    ~Sprite() = default;

    sf::Color color() const;
    void setColor(const sf::Color &color);

    std::shared_ptr<Texture> texture() const;
    void setTexture(const std::shared_ptr<Texture> &texture, bool updateSize = true);

    sf::Vector2i size() const;
    void setSize(int32_t width, int32_t height);
    void setSize(const sf::Vector2i &size);

    AnimatedTextureState *animatedState() const;

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget& target, sf::RenderStates state, sf::Time elapsed) const;

private:
    void updatePositions();
    void updateTexCoords();

private:
    sf::Color m_color;
    std::shared_ptr<Texture> m_texture;
    mutable std::unique_ptr<AnimatedTextureState> m_animatedState;
    sf::VertexArray m_vertices;

};

}

#endif // SGEN_SPRITE_H
