#ifndef SGEN_FONTCACHE_H
#define SGEN_FONTCACHE_H

#include <sgen/common/export.h>
#include <sgen/core/resourcecache.h>

#include <SFML/Graphics/Font.hpp>

namespace sgen {

class Font;

class SGEN_EXPORT FontCache : public ResourceCache<Font>
{
public:
    FontCache();
    ~FontCache() = default;

    bool load(const nlohmann::json &json);

private:
    std::shared_ptr<sf::Font> getSourceFont(const std::string &path);

private:
    std::unordered_map<std::string, std::weak_ptr<sf::Font>> m_fontCache;

};

}

#endif // SGEN_FONTCACHE_H
