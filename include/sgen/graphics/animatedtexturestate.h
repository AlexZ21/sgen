#ifndef SGEN_ANIMATEDTEXTURESTATE_H
#define SGEN_ANIMATEDTEXTURESTATE_H

#include <sgen/common/export.h>
#include <sgen/graphics/animatedtexture.h>

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <memory>
#include <functional>

namespace sgen {

class SGEN_EXPORT AnimatedTextureState
{
public:
    AnimatedTextureState(const std::shared_ptr<AnimatedTexture> &texture);
    ~AnimatedTextureState() = default;

    void play();
    void pause();
    void stop();

    sf::IntRect rect() const;

    void onUpdated(const std::function<void ()> &onUpdated);

    void update(sf::Time elapsed);

    bool isLooped() const;
    void setLooped(bool looped);

    sf::Time frameTime() const;
    void setFrameTime(const sf::Time &frameTime);

private:
    std::shared_ptr<AnimatedTexture> m_texture;

    sf::Time m_elapsedTime;
    sf::Time m_frameTime;

    uint32_t m_currentFrame;

    bool m_paused;
    bool m_looped;

    std::function<void ()> m_onUpdated;

};

}

#endif // SGEN_ANIMATEDTEXTURESTATE_H
