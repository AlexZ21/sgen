#ifndef SGEN_STATICTEXTURE_H
#define SGEN_STATICTEXTURE_H

#include <sgen/common/export.h>
#include <sgen/graphics/texture.h>

#include <memory>

namespace sgen {

class SGEN_EXPORT StaticTexture : public Texture
{
public:
    StaticTexture(const std::shared_ptr<sf::Texture> &source,
                  const sf::IntRect &rect = sf::IntRect());
    ~StaticTexture();

    Type type() const;
    std::shared_ptr<sf::Texture> source() const;
    sf::IntRect rect() const;

    SGEN_RESOURCE_TYPE(TEXTURE_RESOURCE_TYPE)

private:
    std::shared_ptr<sf::Texture> m_source;
    sf::IntRect m_rect;

};

}

#endif // SGEN_STATICTEXTURE_H
