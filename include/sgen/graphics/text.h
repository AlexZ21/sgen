#ifndef SGEN_TEXT_H
#define SGEN_TEXT_H

#include <sgen/common/export.h>
#include <sgen/graphics/drawable.h>
#include <sgen/graphics/font.h>

#include <SFML/System/String.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace sgen {

class SGEN_EXPORT Text : public Drawable
{
public:
    enum Style {
        Regular       = 0,      ///< Regular characters, no style
        Bold          = 1 << 0, ///< Bold characters
        Italic        = 1 << 1, ///< Italic characters
        Underlined    = 1 << 2, ///< Underlined characters
        StrikeThrough = 1 << 3  ///< Strike through characters
    };

    Text();
    Text(const std::shared_ptr<Font> &font);
    Text(const std::shared_ptr<Font> &font, const sf::String &string);
    ~Text() = default;

    void setFont(const std::shared_ptr<Font> &font);
    std::shared_ptr<Font> font() const;

    sf::String string() const;
    void setString(const sf::String &string);

    sf::Vector2i size() const;
    void setSize(int32_t width, int32_t height);
    void setSize(const sf::Vector2i &size);

    bool isWordWrap() const;
    void setWordWrap(bool wordWrap);

    bool isElided() const;
    void setElided(bool elided);

    uint32_t characterSize() const;
    void setCharacterSize(const uint32_t &characterSize);

    float letterSpacingFactor() const;
    void setLetterSpacingFactor(float letterSpacingFactor);

    float lineSpacingFactor() const;
    void setLineSpacingFactor(float lineSpacingFactor);

    Style style() const;
    void setStyle(const Style &style);

    sf::Color fillColor() const;
    void setFillColor(const sf::Color &fillColor);

    sf::Color outlineColor() const;
    void setOutlineColor(const sf::Color &outlineColor);

    float outlineThickness() const;
    void setOutlineThickness(float outlineThickness);

    sf::FloatRect localBounds() const;
    sf::FloatRect globalBounds() const;

    void draw(sf::RenderTarget& target, sf::RenderStates states, sf::Time elapsed) const;

private:
    void ensureGeometryUpdate() const;

private:
    std::shared_ptr<Font> m_font;
    sf::String m_string;
    sf::Vector2i m_size;
    bool m_wordWrap;
    bool m_elided;
    uint32_t m_characterSize;
    float m_letterSpacingFactor;
    float m_lineSpacingFactor;
    Style m_style;
    sf::Color m_fillColor;
    sf::Color m_outlineColor;
    float m_outlineThickness;
    mutable sf::VertexArray m_vertices;
    mutable sf::VertexArray m_outlineVertices;
    mutable sf::FloatRect m_bounds;
    mutable bool m_geometryNeedUpdate;
    mutable ResourceId m_fontResourceId;
};
}

#endif // SGEN_TEXT_H
