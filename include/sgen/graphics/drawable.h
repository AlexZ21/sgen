#ifndef SGEN_DRAWABLE_H
#define SGEN_DRAWABLE_H

#include <sgen/common/export.h>
#include <sgen/graphics/transformable.h>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/System/Time.hpp>

namespace sgen {

class SGEN_EXPORT Drawable : public Transformable
{
public:
    Drawable();
    ~Drawable() = default;

    float zIndex() const;
    void setZIndex(float zIndex);

    virtual sf::FloatRect localBounds() const = 0;
    virtual sf::FloatRect globalBounds() const = 0;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states, sf::Time elapsed) const = 0;

private:
    float m_zIndex;

};

}

#endif // SGEN_DRAWABLE_H
