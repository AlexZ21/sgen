#ifndef SGEN_TEXTURECACHE_H
#define SGEN_TEXTURECACHE_H

#include <sgen/common/export.h>
#include <sgen/core/resourcecache.h>

#include <SFML/Graphics/Texture.hpp>

#include <unordered_map>
#include <memory>

namespace sgen {

class Texture;
class StaticTexture;
class AnimatedTexture;

class SGEN_EXPORT TextureCache : public ResourceCache<Texture,
                                                      StaticTexture,
                                                      AnimatedTexture>
{
public:
    enum TextureType {
        STATIC_TEXTURE = 0,
        ANIMATED_TEXTURE = 1
    };

    TextureCache();
    ~TextureCache() = default;

    bool load(const nlohmann::json &json);

private:
    bool createStaticTexture(const nlohmann::json &json);
    bool createAnimatedTexture(const nlohmann::json &json);
    std::shared_ptr<sf::Texture> getSourceTexture(const std::string &path);

private:
    std::unordered_map<std::string, std::weak_ptr<sf::Texture>> m_sourceCache;

};


}

#endif // SGEN_TEXTURECACHE_H
