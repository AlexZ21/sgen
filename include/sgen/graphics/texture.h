#ifndef SGEN_TEXTURE_H
#define SGEN_TEXTURE_H

#include <sgen/common/export.h>
#include <sgen/core/resource.h>

#include <SFML/Graphics/Texture.hpp>

#include <memory>

namespace sgen {

class SGEN_EXPORT Texture : public Resource
{
public:

    enum Type {
        Static = 0,
        Animated
    };

    Texture() : Resource() {}
    ~Texture() = default;

    virtual Type type() const { return Static; }
    virtual std::shared_ptr<sf::Texture> source() const { return nullptr; }
    virtual sf::IntRect rect() const { return sf::IntRect(); }

    SGEN_RESOURCE_TYPE(TEXTURE_RESOURCE_TYPE)

};

}

#endif // SGEN_TEXTURE_H
