#ifndef SGEN_FONT_H
#define SGEN_FONT_H

#include <sgen/common/export.h>
#include <sgen/core/resource.h>

#include <SFML/Graphics/Font.hpp>

#include <memory>

namespace sgen {

class SGEN_EXPORT Font : public Resource
{
public:
    Font(const std::shared_ptr<sf::Font> &font);
    ~Font() = default;

    std::shared_ptr<sf::Font> source() const { return m_source; }

    SGEN_RESOURCE_TYPE(FONT_RESOURCE_TYPE)

private:
    std::shared_ptr<sf::Font> m_source;

};

}

#endif // SGEN_FONT_H
