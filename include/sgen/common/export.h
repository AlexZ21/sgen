#ifndef SGEN_EXPORT_H
#define SGEN_EXPORT_H

#if !defined(SGEN_STATIC)
    #if defined(SYSTEM_WIN)
        #define SGEN_API_EXPORT __declspec(dllexport)
        #define SGEN_API_IMPORT __declspec(dllimport)

        #ifdef _MSC_VER
            #pragma warning(disable: 4251)
        #endif

    #else // Linux, FreeBSD, Mac OS X
        #if __GNUC__ >= 4
            #define SGEN_API_EXPORT __attribute__ ((__visibility__ ("default")))
            #define SGEN_API_IMPORT __attribute__ ((__visibility__ ("default")))
        #else
            #define SGEN_API_EXPORT
            #define SGEN_API_IMPORT
        #endif
    #endif
#else
    #define SGEN_API_EXPORT
    #define SGEN_API_IMPORT
#endif

#if !defined(SGEN_STATIC)
#  define SGEN_EXPORT SGEN_API_EXPORT
#else
#  define SGEN_EXPORT SGEN_API_IMPORT
#endif

#endif // SGEN_EXPORT_H
