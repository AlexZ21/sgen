#ifndef SGEN_CONSTS_H
#define SGEN_CONSTS_H

#include <stdint.h>
#include <string>

namespace sgen {

using ResourceId = std::string;
const ResourceId INVALID_RESOURCE_ID;

using ResourceType = int32_t;
const ResourceType INVALID_RESOURCE_TYPE = -1;
const ResourceType TEXTURE_RESOURCE_TYPE = 1000;
const ResourceType FONT_RESOURCE_TYPE = 1001;
const ResourceType GUI_THEME_RESOURCE_TYPE = 1100;

}

#endif // SGEN_CONSTS_H
