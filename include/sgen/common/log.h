#ifndef SGEN_LOG_H
#define SGEN_LOG_H

#include <sgen/common/export.h>

#include <iostream>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <string>

#define LENABLE             ::sgen::Log::enable()
#define LDISABLE            ::sgen::Log::disable()
#define LFILTER(_filter_)   ::sgen::Log::setFilter(_filter_)
#define LDEBUG(...)         ::sgen::Log::debug(__VA_ARGS__)
#define LSDEBUG(...)        ::sgen::Log::debug_s(__VA_ARGS__)
#define LINFO(...)          ::sgen::Log::info(__VA_ARGS__)
#define LSINFO(...)         ::sgen::Log::info_s(__VA_ARGS__)
#define LWARN(...)          ::sgen::Log::warning(__VA_ARGS__)
#define LSWARN(...)         ::sgen::Log::warning_s(__VA_ARGS__)
#define LERR(...)           ::sgen::Log::error(__VA_ARGS__)
#define LSERR(...)          ::sgen::Log::error_s(__VA_ARGS__)
#define LFATAL(...)         ::sgen::Log::fatal(__VA_ARGS__)
#define LSFATAL(...)        ::sgen::Log::fatal_s(__VA_ARGS__)

#define LTOSTR(_expr_)      #_expr_

inline std::string classMethodName(const std::string &s)
{
    size_t n = s.find("(");
    if (n == std::string::npos)
        return "unknow";

    size_t methodStartPos = s.rfind("::", n);
    size_t classStartPos = 0;

    if (methodStartPos == std::string::npos) {
        classStartPos = 0;
    } else {
        if ((methodStartPos - 1) > 1) {
            classStartPos = s.rfind("::", methodStartPos - 1);
            if (classStartPos == std::string::npos)
                classStartPos = 0;
            else
                classStartPos += 2;
        }
    }

    if (classStartPos == 0) {
        classStartPos = s.find(" ");
        if (classStartPos == std::string::npos || classStartPos >= n)
            classStartPos = 0;
        else
            classStartPos += 1;
    }

    return s.substr(classStartPos, n - (classStartPos));
}

#define LOG_CLASS_METHOD classMethodName(__PRETTY_FUNCTION__)

namespace sgen {

class SGEN_EXPORT Log
{
public:
    enum Level {
        DEBUG = 0x0002,
        INFO = 0x0004,
        WARNING = 0x0008,
        ERROR = 0x00012,
        FATAL = 0x0016
    };

    template <typename... Args>
    static void debug(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & DEBUG))
            return;
        log.push("DEBUG", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void debug_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & DEBUG))
            return;
        log.push("DEBUG", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & INFO))
            return;
        log.push("INFO", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void info_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & INFO))
            return;
        log.push("INFO", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & WARNING))
            return;
        log.push("WARN", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void warning_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & WARNING))
            return;
        log.push("WARN", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & ERROR))
            return;
        log.push("ERROR", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void error_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & ERROR))
            return;
        log.push("ERROR", true, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & FATAL))
            return;
        log.push("FATAL", false, std::forward<Args>(args)...);
    }

    template <typename... Args>
    static void fatal_s(Args &&...args) {
        Log &log = instance();
        if (!log.m_enable || !(log.m_filter & FATAL))
            return;
        log.push("FATAL", true, std::forward<Args>(args)...);
    }

    static void enable() {
        instance().m_enable = true;
    }
    static void disable() {
        instance().m_enable = false;
    }
    static void setFilter(int filter) {
        instance().m_filter = filter;
    }

private:
    static Log &instance();

    Log() : m_enable(false), m_filter(DEBUG | INFO | WARNING | ERROR | FATAL) {}
    ~Log() = default;

    template <typename... Args>
    void push(const std::string &level, bool spacing, Args &&...args) {
        std::string s = spacing ? " " : "";
        m_stream << '[' << currentTime() << ']';
        m_stream << '[' << level << ']' << "\t ";
        using expander = int[];
        (void)expander{0, (void(m_stream << s << std::forward<Args>(args)), 0)...};
        print();
    }

    template <typename Arg>
    void push(std::stringstream &stream, Arg &&arg) {
        stream << std::forward<Arg>(arg);
    }

    std::string currentTime() const {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        std::time_t now_c = std::chrono::system_clock::to_time_t(now - std::chrono::hours(24));
        std::string currentTime = std::ctime(&now_c);
        currentTime.pop_back();
        return currentTime;
    }

    void print() {
        std::cout << m_stream.str() << std::endl;
        m_stream.str(std::string());
        m_stream.clear();
    }

private:
    std::stringstream m_stream;
    bool m_enable;
    int m_filter;

};

}

#endif // SGEN_LOG_H
