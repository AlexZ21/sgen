#ifndef SGEN_SCENESYSTEM_H
#define SGEN_SCENESYSTEM_H

#include <sgen/common/export.h>
#include <sgen/core/system.h>
#include <sgen/components/scenecomponent.h>
#include <sgen/graphics/aabbtree.h>

namespace sgen {

class SGEN_EXPORT SceneSystem : public FilteredSystem<SceneComponent>
{
public:
    SceneSystem();
    ~SceneSystem() = default;

    void update(int32_t milliseconds);

    const AABBTree<Entity> &entityTree() const;

    SGEN_SYSTEM_TYPE_ID(SceneSystem)

protected:
    void onEntityAdded(const Entity &entity);
    void onEntityRemoved(const Entity &entity);

    void updateTree(SceneComponent *sceneComponent);

private:
    AABBTree<Entity> m_entityTree;

};

}

#endif // SGEN_SCENESYSTEM_H
