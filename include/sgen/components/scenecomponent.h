#ifndef SGEN_SCENECOMPONENT_H
#define SGEN_SCENECOMPONENT_H

#include <sgen/common/export.h>
#include <sgen/core/component.h>
#include <sgen/core/idpool.h>
#include <sgen/graphics/drawable.h>

#include <memory>

namespace sgen {

class SceneSystem;

class SGEN_EXPORT SceneComponent : public Component
{
    friend class SceneSystem;
public:
    SceneComponent();
    ~SceneComponent() = default;

    Id treeId() const;
    void setTreeId(const Id &treeId);

    template<typename T>
    std::shared_ptr<T> drawable() const {
        return std::static_pointer_cast<T>(drawable());
    }

    std::shared_ptr<Drawable> drawable() const;
    void setDrawable(const std::shared_ptr<Drawable> &drawable);
    sf::FloatRect globalBounds() const;

    bool isChanged();

    SGEN_COMPONENT_TYPE_ID(SceneComponent)

private:
    Id m_treeId;

    std::shared_ptr<Drawable> m_drawable;
    sf::FloatRect m_globalBounds;

};

}

#endif // SGEN_SCENECOMPONENT_H
