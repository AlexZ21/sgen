#include <sgen/systems/scenesystem.h>
#include <sgen/common/log.h>

namespace sgen {

SceneSystem::SceneSystem()
{

}

void SceneSystem::update(int32_t milliseconds)
{
    (void)(milliseconds);

    for (const Entity &entity : entities()) {
        SceneComponent *sceneComponent = entity.component<SceneComponent>();
        if (sceneComponent) {
            // Update tree node if need
            if (sceneComponent->isChanged()) {
                updateTree(sceneComponent);
            }
        }
    }
}

void SceneSystem::onEntityAdded(const Entity &entity)
{
    SceneComponent *sceneComponent = entity.component<SceneComponent>();
    if (sceneComponent) {
        AABB aabb = sceneComponent->drawable() ? sceneComponent->drawable()->globalBounds() : AABB();
        Id id = m_entityTree.insertObject(aabb, entity);
        sceneComponent->setTreeId(id);
    }

}

void SceneSystem::onEntityRemoved(const Entity &entity)
{
    SceneComponent *sceneComponent = entity.component<SceneComponent>();
    if (sceneComponent) {
        m_entityTree.removeObject(sceneComponent->treeId());
    }
}

void SceneSystem::updateTree(SceneComponent *sceneComponent)
{
    m_entityTree.updateObject(sceneComponent->treeId(), sceneComponent->globalBounds());
}

const AABBTree<Entity> &SceneSystem::entityTree() const
{
    return m_entityTree;
}

}
