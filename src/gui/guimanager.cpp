#include <sgen/gui/guimanager.h>
#include <sgen/gui/widget.h>
#include <sgen/graphics/clipping.h>
#include <sgen/common/log.h>

#include <limits>

namespace sgen {

GuiManager::GuiManager() :
      EventHandler({EventTypes::KeyPressed,
                    EventTypes::Closed,
                    EventTypes::Resized,
                    EventTypes::LostFocus,
                    EventTypes::GainedFocus,
                    EventTypes::TextEntered,
                    EventTypes::KeyPressed,
                    EventTypes::KeyReleased,
                    EventTypes::MouseWheelMoved,
                    EventTypes::MouseWheelScrolled,
                    EventTypes::MouseButtonPressed,
                    EventTypes::MouseButtonReleased,
                    EventTypes::MouseMoved,
                    EventTypes::MouseEntered,
                    EventTypes::MouseLeft,
                    EventTypes::JoystickButtonPressed,
                    EventTypes::JoystickButtonReleased,
                    EventTypes::JoystickMoved,
                    EventTypes::JoystickConnected,
                    EventTypes::JoystickDisconnected,
                    EventTypes::TouchBegan,
                    EventTypes::TouchMoved,
                    EventTypes::TouchEnded,
                    EventTypes::SensorChanged},
                   std::numeric_limits<int32_t>::max()),
      m_view({0, 0, 1, 1})
{
}

bool GuiManager::event(const Event *event)
{
    if (event->type == EventTypes::Resized) {
        if (m_rootWidget) {
            const ResizedEvent *e = static_cast<const ResizedEvent *>(event);

            m_view.setSize({static_cast<float>(e->width), static_cast<float>(e->height)});
            m_view.setCenter({static_cast<float>(e->width) / 2, static_cast<float>(e->height) / 2});

            m_rootWidget->event(event);
            return false;
        }
    }

    if (event->type == EventTypes::MouseMoved) {
        if (m_rootWidget) {
            const MouseMovedEvent *e = static_cast<const MouseMovedEvent *>(event);

            std::shared_ptr<Widget> hoveredWidget = widgetAtPoint(e->x, e->y, m_rootWidget);
            if (hoveredWidget && !hoveredWidget->isIgnoreMouseHover()) {
                if (hoveredWidget != m_hoveredWidget) {
                    if (m_hoveredWidget)
                        m_hoveredWidget->setMouseHover(false);
                    m_hoveredWidget = hoveredWidget;
                    m_hoveredWidget->setMouseHover(true);
                    m_hoveredWidget->event(event);
                } else {
                    m_hoveredWidget->event(event);
                }
            }

            if (m_downedWidget) {
                if (m_downedWidget->isDraggable() && m_downedWidget->isMouseDown() && m_downedWidget != m_draggedWidget) {
                    m_draggedWidget = m_downedWidget;
                    m_draggedPointStart = m_draggedWidget->globalToLocal(e->x, e->y);
                    setRecursiveIgnoreMouseHover(m_draggedWidget, true);
                }
            }

            if (m_draggedWidget)
                m_draggedPointCurrent = {e->x - m_draggedPointStart.x, e->y - m_draggedPointStart.y};

            return false;
        }
    }

    if (event->type == EventTypes::MouseButtonPressed) {
        if (m_hoveredWidget) {
            m_downedWidget = m_hoveredWidget;
            m_downedWidget->setMouseDown(true);
            m_downedWidget->event(event);
        }
        return false;
    }

    if (event->type == EventTypes::MouseButtonReleased) {
        if (m_downedWidget) {
            m_downedWidget->setMouseDown(false);
            m_downedWidget->event(event);
            m_downedWidget.reset();
        }

        if (m_draggedWidget) {
            const MouseButtonReleasedEvent *e = static_cast<const MouseButtonReleasedEvent *>(event);
            std::shared_ptr<Widget> widgetUnderPoint = widgetAtPoint(e->x, e->y, m_rootWidget);

            if (widgetUnderPoint) {
                if (widgetUnderPoint == m_draggedWidget || m_draggedWidget->isChild(widgetUnderPoint)) {
                    std::shared_ptr<Widget> draggedWidgetParent = m_draggedWidget->m_parent.lock();
                    if (draggedWidgetParent) {
                        sf::Vector2i dropPoint = draggedWidgetParent->globalToLocal(e->x, e->y);
                        draggedWidgetParent->dropWidget(dropPoint, m_draggedPointStart, m_draggedWidget);
                    }
                } else {
                    sf::Vector2i dropPoint = widgetUnderPoint->globalToLocal(e->x, e->y);
                    widgetUnderPoint->dropWidget(dropPoint, m_draggedPointStart, m_draggedWidget);
                }
            }

            setRecursiveIgnoreMouseHover(m_draggedWidget, false);
            m_draggedWidget.reset();
        }

        return false;
    }

    return false;
}

void GuiManager::draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed)
{
    const sf::View oldView = target.getView();

    target.setView(m_view);
    Clipping::setOriginalView(m_view);

    if (m_rootWidget)
        m_rootWidget->drawWidget(target, states, elapsed);

    if (m_draggedWidget) {
        sf::Vector2f position = m_draggedWidget->position();
        m_draggedWidget->setPosition(m_draggedPointCurrent.x, m_draggedPointCurrent.y);
        m_draggedWidget->drawWidget(target, states, elapsed);
        m_draggedWidget->setPosition(position);
    }

    target.setView(oldView);
}

std::shared_ptr<Widget> GuiManager::rootWidget() const
{
    return m_rootWidget;
}

void GuiManager::setRootWidget(const std::shared_ptr<Widget> &rootWidget)
{
    m_rootWidget = rootWidget;
    m_rootWidget->setTheme(m_theme);
}

std::shared_ptr<GuiTheme> GuiManager::theme() const
{
    return m_theme;
}

void GuiManager::setTheme(const std::shared_ptr<GuiTheme> &theme, bool updateChildren)
{
    m_theme = theme;
    if (updateChildren && m_rootWidget)
        m_rootWidget->setTheme(m_theme, updateChildren);
}

std::shared_ptr<Widget> GuiManager::widgetAtPoint(int32_t x, int32_t y, const std::shared_ptr<Widget> &widget)
{
    if (widget) {
        sf::FloatRect bounds = widget->globalBounds();

        if (bounds.contains(x, y)) {
            for (std::shared_ptr<Widget> &child : widget->m_children) {
                std::shared_ptr<Widget> finded = widgetAtPoint(x, y, child);
                if (finded)
                    return finded;
            }

            return widget;
        }
    }

    return nullptr;
}

void GuiManager::setRecursiveIgnoreMouseHover(const std::shared_ptr<Widget> &widget, bool ignore)
{
    widget->setIgnoreMouseHover(ignore);
    for (std::shared_ptr<Widget> &child : widget->m_children)
        setRecursiveIgnoreMouseHover(child, ignore);
}

}
