#include <sgen/gui/layout.h>
#include <sgen/gui/widget.h>

#include <algorithm>

namespace sgen {

void Layout::WidgetItem::updateContentGeometry(const sf::Vector2i &position, const sf::Vector2i &size)
{
    widget->setPosition(position.x, position.y);
    widget->setSize(size);
}

sf::Vector2i Layout::WidgetItem::minimumSize()
{
    return widget->minimumSize();
}

void Layout::LayoutItem::updateContentGeometry(const sf::Vector2i &position, const sf::Vector2i &size)
{
    layout->setPosition(position);
    layout->setSize(size);
    layout->updateContentGeometry();
}

sf::Vector2i Layout::LayoutItem::minimumSize()
{
    return layout->minimumSize();
}

Layout::Layout() :
      p_maxFactor(0),
      p_maxSpacing(0),
      p_spacing(5),
      p_position({0, 0}),
      p_size({0, 0})
{

}

void Layout::addWidget(const std::shared_ptr<Widget> &widget, int32_t strechFactor)
{
    if (!widget || findWidget(widget))
        return;

    std::shared_ptr<WidgetItem> item = std::make_shared<WidgetItem>();
    item->widget = widget;
    item->strechFactor = strechFactor < 1 ? 1 :strechFactor;
    p_items.push_back(item);
    p_maxFactor += item->strechFactor;
    updateContentGeometry();
}

void Layout::addLayout(const std::shared_ptr<Layout> &layout, int32_t strechFactor)
{
    if (!layout || findLayout(layout))
        return;

    std::shared_ptr<LayoutItem> item = std::make_shared<LayoutItem>();
    item->layout = layout;
    item->strechFactor = strechFactor < 1 ? 1 : strechFactor;
    p_items.push_back(item);
    p_maxFactor += item->strechFactor;
    updateContentGeometry();
}

void Layout::addStrech(int32_t strechFactor)
{
    std::shared_ptr<StrechItem> item = std::make_shared<StrechItem>();
    item->strechFactor = strechFactor < 1 ? 1 :strechFactor;
    p_items.push_back(item);
    p_maxFactor += item->strechFactor;
    updateContentGeometry();
}

void Layout::addSpacing(int32_t space)
{
    if (space <= 0)
        return;

    std::shared_ptr<SpacingItem> item = std::make_shared<SpacingItem>();
    item->strechFactor = 0;
    item->space = space;
    p_items.push_back(item);
    p_maxSpacing += item->space;
    updateContentGeometry();
}

void Layout::removeItem(int32_t index)
{
    if (index >= p_items.size())
        return;
    p_items.erase(p_items.begin() + index);
    updateContentGeometry();
}

const std::vector<std::shared_ptr<Layout::Item> > &Layout::items() const
{
    return p_items;
}

std::shared_ptr<Layout::Item> Layout::findWidget(const std::shared_ptr<Widget> &widget) const
{
    auto it = std::find_if(p_items.begin(), p_items.end(),
                           [widget](const std::shared_ptr<Item> &item) {
                               if (item->type == Item::WIDGET) {
                                   std::shared_ptr<WidgetItem> i = std::static_pointer_cast<WidgetItem>(item);
                                   return i->widget == widget;
                               }
                               return false;
                           });

    if (it != p_items.end())
        return *it;

    return nullptr;
}

std::shared_ptr<Layout::Item> Layout::findLayout(const std::shared_ptr<Layout> &layout) const
{
    auto it = std::find_if(p_items.begin(), p_items.end(),
                           [layout](const std::shared_ptr<Item> &item) {
                               if (item->type == Item::LAYOUT) {
                                   const std::shared_ptr<LayoutItem> i = std::static_pointer_cast<LayoutItem>(item);
                                   return i->layout == layout;
                               }
                               return false;
                           });

    if (it != p_items.end())
        return *it;

    return nullptr;
}

int32_t Layout::spacing() const
{
    return p_spacing;
}

void Layout::setSpacing(const int32_t &value)
{
    p_spacing = value;
}

void Layout::setPosition(const sf::Vector2i &position)
{
    p_position = position;
}

void Layout::setSize(const sf::Vector2i &size)
{
    p_size = size;
}

}
