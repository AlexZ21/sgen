#include <sgen/gui/guitheme.h>

namespace sgen {

GuiTheme::GuiTheme() :
      Resource()
{

}

std::shared_ptr<WidgetStyle> GuiTheme::style(int32_t styleType) const
{
    auto it = m_styles.find(styleType);
    if (it == m_styles.end())
        return nullptr;
    return  it->second;
}

void GuiTheme::setStyle(const std::shared_ptr<WidgetStyle> &style)
{
    if (!style)
        return;
    m_styles.emplace(style->type, style);
}

}
