#include <sgen/gui/hlayout.h>
#include <sgen/common/log.h>

namespace sgen {

HLayout::HLayout()
{

}

void HLayout::updateContentGeometry()
{
    if (p_items.empty())
        return;

    int32_t contentsWidth = static_cast<int32_t>(p_size.x - ((p_spacing * (p_items.size() - 1)) + p_maxSpacing));
    int32_t minimumWidth = minimumSize().y;
    contentsWidth = contentsWidth > minimumWidth ? contentsWidth : minimumWidth;
    int32_t contentHeight = std::max(minimumSize().y, p_size.y);
    int32_t nextPosition = p_position.x;

    for (std::shared_ptr<Item> &item : p_items) {
        if (item->type != Item::SPACING) {
            int32_t itemWidth = static_cast<int32_t>(contentsWidth * ((float)item->strechFactor / p_maxFactor));
            item->updateContentGeometry({nextPosition, p_position.y}, {itemWidth, contentHeight});
            nextPosition += (itemWidth + p_spacing);
        } else {
            std::shared_ptr<SpacingItem> spacingItem = std::static_pointer_cast<SpacingItem>(item);
            nextPosition += (spacingItem->space + p_spacing);
        }
    }

}

sf::Vector2i HLayout::minimumSize()
{
    int32_t minimumWidth = static_cast<int32_t>((p_spacing * (p_items.size() - 1)));
    int32_t minimumHeight = 0;
    for (std::shared_ptr<Item> &item : p_items) {
        minimumWidth += item->minimumSize().x;
        if (item->minimumSize().y > minimumHeight)
            minimumHeight = item->minimumSize().y;
    }
    return {minimumWidth, minimumHeight};
}

}
