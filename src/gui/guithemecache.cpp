#include <sgen/gui/guithemecache.h>
#include <sgen/gui/guitheme.h>
#include <sgen/core/resourcemanager.h>
#include <sgen/core/game.h>
#include <sgen/common/log.h>

namespace sgen {

bool GuiThemeCache::load(const nlohmann::json &json)
{
    try {
        ResourceType type = json.at("resource_type").get<ResourceType>();
        ResourceId id = json.at("resource_id").get<ResourceId>();

        std::shared_ptr<GuiTheme> theme = std::make_shared<GuiTheme>();
        theme->setResourceId(id);

        const nlohmann::json &stylesJson = json.at("styles");

        for (const nlohmann::json &styleJson : stylesJson) {
            int32_t styleType = styleJson.at("type").get<int32_t>();

            switch (styleType) {
            case WidgetStyleTypes::FrameWidget:
            {
                std::shared_ptr<FrameWidgetStyle> style = std::make_shared<FrameWidgetStyle>();

                const nlohmann::json &statesJson = styleJson.at("states");
                for (const auto &item : statesJson.items()) {
                    FrameWidgetStyle::State state;
                    std::string stateName = item.key();

                    std::string textureId = item.value().at("texture").get<std::string>();
                    state.texture = Game::instance()->resourceManager()->get<Texture>(textureId);

                    state.rect = {item.value().at("rect").at(0).get<int32_t>(),
                                  item.value().at("rect").at(1).get<int32_t>(),
                                  item.value().at("rect").at(2).get<int32_t>(),
                                  item.value().at("rect").at(3).get<int32_t>()};
                    state.borders = {item.value().at("borders").at(0).get<int32_t>(),
                                     item.value().at("borders").at(1).get<int32_t>(),
                                     item.value().at("borders").at(2).get<int32_t>(),
                                     item.value().at("borders").at(3).get<int32_t>()};
                    style->states.emplace(stateName, state);
                }

                theme->setStyle(style);
                break;
            }
            default:
                break;
            }

        }

        return add(theme);

    } catch (const std::exception &e) {
        LWARN("Error loading resource from json: ", e.what());
        return false;
    }
}

}
