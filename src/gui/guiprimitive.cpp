#include <sgen/gui/guiprimitive.h>
#include <sgen/common/log.h>

#define LEFT_BORDER m_borders[0]
#define TOP_BORDER m_borders[1]
#define RIGHT_BORDER m_borders[2]
#define BOTTOM_BORDER m_borders[3]

namespace sgen {

FramedPrimitive::FramedPrimitive() :
      m_vertices(sf::Quads),
      m_size({1,1}),
      m_borders({1, 1, 1, 1})
{
    m_vertices.resize(36);
}

void FramedPrimitive::setSize(const sf::Vector2i &size)
{
    m_size = size;
    updateVericesPostion();
}

void FramedPrimitive::setSize(int32_t width, int32_t height)
{
    setSize({width, height});
}

void FramedPrimitive::setTexturePosition(const sf::IntRect &rect, const std::vector<int32_t> &borders)
{
    m_rect = rect;
    m_borders = borders;

    float left = static_cast<float>(m_rect.left);
    float right = left + m_rect.width;
    float top = static_cast<float>(m_rect.top);
    float bottom = top + m_rect.height;

    if ((left + LEFT_BORDER > right - RIGHT_BORDER) || (top + TOP_BORDER > bottom - BOTTOM_BORDER) ||
        ((LEFT_BORDER + RIGHT_BORDER) > m_size.x) || ((TOP_BORDER + BOTTOM_BORDER) > m_size.y)) {
        for (uint64_t i = 0; i < 36; ++i) {
            m_vertices[i].texCoords = sf::Vector2f(0, 0);
        }
        return;
    } else {
        updateVericesPostion();
        updateTexturePosition();
    }
}

void FramedPrimitive::draw(sf::RenderTarget &target, sf::RenderStates state) const
{
    target.draw(m_vertices, state);
}

void FramedPrimitive::updateVericesPostion()
{    
    // Left top corner
    m_vertices[0].position = sf::Vector2f(0, TOP_BORDER);
    m_vertices[1].position = sf::Vector2f(0, 0);
    m_vertices[2].position = sf::Vector2f(LEFT_BORDER, 0);
    m_vertices[3].position = sf::Vector2f(LEFT_BORDER, TOP_BORDER);

    // Top border
    m_vertices[4].position = sf::Vector2f(LEFT_BORDER, TOP_BORDER);
    m_vertices[5].position = sf::Vector2f(LEFT_BORDER, 0);
    m_vertices[6].position = sf::Vector2f(m_size.x - RIGHT_BORDER, 0);
    m_vertices[7].position = sf::Vector2f(m_size.x - RIGHT_BORDER, TOP_BORDER);

    // Right top border
    m_vertices[8].position = sf::Vector2f(m_size.x - RIGHT_BORDER, TOP_BORDER);
    m_vertices[9].position = sf::Vector2f(m_size.x - RIGHT_BORDER, 0);
    m_vertices[10].position = sf::Vector2f(m_size.x, 0);
    m_vertices[11].position = sf::Vector2f(m_size.x, TOP_BORDER);

    // Right border
    m_vertices[12].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[13].position = sf::Vector2f(m_size.x - RIGHT_BORDER, TOP_BORDER);
    m_vertices[14].position = sf::Vector2f(m_size.x, TOP_BORDER);
    m_vertices[15].position = sf::Vector2f(m_size.x, m_size.y - BOTTOM_BORDER);

    // Right bottom corner
    m_vertices[16].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y);
    m_vertices[17].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[18].position = sf::Vector2f(m_size.x, m_size.y - BOTTOM_BORDER);
    m_vertices[19].position = sf::Vector2f(m_size.x, m_size.y);

    // Bottom border
    m_vertices[20].position = sf::Vector2f(LEFT_BORDER, m_size.y);
    m_vertices[21].position = sf::Vector2f(LEFT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[22].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[23].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y);

    // Left bottom corner
    m_vertices[24].position = sf::Vector2f(0, m_size.y);
    m_vertices[25].position = sf::Vector2f(0, m_size.y - BOTTOM_BORDER);
    m_vertices[26].position = sf::Vector2f(LEFT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[27].position = sf::Vector2f(LEFT_BORDER, m_size.y);

    // Left border
    m_vertices[28].position = sf::Vector2f(0, m_size.y - BOTTOM_BORDER);
    m_vertices[29].position = sf::Vector2f(0, TOP_BORDER);
    m_vertices[30].position = sf::Vector2f(LEFT_BORDER, TOP_BORDER);
    m_vertices[31].position = sf::Vector2f(LEFT_BORDER, m_size.y - BOTTOM_BORDER);

    // Center
    m_vertices[32].position = sf::Vector2f(LEFT_BORDER, m_size.y - BOTTOM_BORDER);
    m_vertices[33].position = sf::Vector2f(LEFT_BORDER, TOP_BORDER);
    m_vertices[34].position = sf::Vector2f(m_size.x - RIGHT_BORDER, TOP_BORDER);
    m_vertices[35].position = sf::Vector2f(m_size.x - RIGHT_BORDER, m_size.y - BOTTOM_BORDER);
}

void FramedPrimitive::updateTexturePosition()
{
    float left = static_cast<float>(m_rect.left);
    float right = left + m_rect.width;
    float top = static_cast<float>(m_rect.top);
    float bottom = top + m_rect.height;

    // Left top corner
    m_vertices[0].texCoords = sf::Vector2f(left, top + TOP_BORDER);
    m_vertices[1].texCoords = sf::Vector2f(left, top);
    m_vertices[2].texCoords = sf::Vector2f(left + LEFT_BORDER, top);
    m_vertices[3].texCoords = sf::Vector2f(left + LEFT_BORDER, top + TOP_BORDER);

    // Top border
    m_vertices[4].texCoords = sf::Vector2f(left + LEFT_BORDER, top + TOP_BORDER);
    m_vertices[5].texCoords = sf::Vector2f(left + LEFT_BORDER, top);
    m_vertices[6].texCoords = sf::Vector2f(right - RIGHT_BORDER, top);
    m_vertices[7].texCoords = sf::Vector2f(right - RIGHT_BORDER, top + TOP_BORDER);

    // Right top border
    m_vertices[8].texCoords = sf::Vector2f(right - RIGHT_BORDER, top + TOP_BORDER);
    m_vertices[9].texCoords = sf::Vector2f(right - RIGHT_BORDER, top);
    m_vertices[10].texCoords = sf::Vector2f(right, top);
    m_vertices[11].texCoords = sf::Vector2f(right, top + TOP_BORDER);

    // Right border
    m_vertices[12].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[13].texCoords = sf::Vector2f(right - RIGHT_BORDER, top + TOP_BORDER);
    m_vertices[14].texCoords = sf::Vector2f(right, top + TOP_BORDER);
    m_vertices[15].texCoords = sf::Vector2f(right, bottom - BOTTOM_BORDER);

    // Right bottom corner
    m_vertices[16].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom);
    m_vertices[17].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[18].texCoords = sf::Vector2f(right, bottom - BOTTOM_BORDER);
    m_vertices[19].texCoords = sf::Vector2f(right, bottom);

    // Bottom border
    m_vertices[20].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom);
    m_vertices[21].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[22].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[23].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom);

    // Left bottom corner
    m_vertices[24].texCoords = sf::Vector2f(left, bottom);
    m_vertices[25].texCoords = sf::Vector2f(left, bottom - BOTTOM_BORDER);
    m_vertices[26].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[27].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom);

    // Left border
    m_vertices[28].texCoords = sf::Vector2f(left, bottom - BOTTOM_BORDER);
    m_vertices[29].texCoords = sf::Vector2f(left, top + TOP_BORDER);
    m_vertices[30].texCoords = sf::Vector2f(left + LEFT_BORDER, top + TOP_BORDER);
    m_vertices[31].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom - BOTTOM_BORDER);

    // Center
    m_vertices[32].texCoords = sf::Vector2f(left + LEFT_BORDER, bottom - BOTTOM_BORDER);
    m_vertices[33].texCoords = sf::Vector2f(left + LEFT_BORDER, top + TOP_BORDER);
    m_vertices[34].texCoords = sf::Vector2f(right - RIGHT_BORDER, top + TOP_BORDER);
    m_vertices[35].texCoords = sf::Vector2f(right - RIGHT_BORDER, bottom - BOTTOM_BORDER);
}

}
