#include <sgen/gui/framedwidget.h>
#include <sgen/graphics/clipping.h>
#include <sgen/core/resourcemanager.h>
#include <sgen/core/game.h>

namespace sgen {

FramedWidget::FramedWidget() :
      Widget()
{
    setMinimumSize(64, 64);

    resized.connect([this](int32_t width, int32_t height){
        m_framedPrimitive.setSize(width, height);
    });

    themeChanged.connect([this](){
        std::shared_ptr<FrameWidgetStyle> style = std::static_pointer_cast<FrameWidgetStyle>(theme()->style(WidgetStyleTypes::FrameWidget));
        const FrameWidgetStyle::State &state = style->states["normal"];
        m_framedPrimitive.setTexturePosition(state.rect, state.borders);
    });

    enableChanged.connect([this](bool enable){
        std::shared_ptr<FrameWidgetStyle> style = std::static_pointer_cast<FrameWidgetStyle>(theme()->style(WidgetStyleTypes::FrameWidget));
        FrameWidgetStyle::State *widgetState = enable ? &style->states["normal"] : &style->states["disabled"];
        m_framedPrimitive.setTexturePosition(widgetState->rect, widgetState->borders);
    });

    mouseHoverChanged.connect([this](bool hovered){
        std::shared_ptr<FrameWidgetStyle> style = std::static_pointer_cast<FrameWidgetStyle>(theme()->style(WidgetStyleTypes::FrameWidget));
        FrameWidgetStyle::State *widgetState = hovered ? &style->states["hovered"] : &style->states["normal"];
        m_framedPrimitive.setTexturePosition(widgetState->rect, widgetState->borders);
    });

    mouseDownChanged.connect([this](bool downed){
        std::shared_ptr<FrameWidgetStyle> style = std::static_pointer_cast<FrameWidgetStyle>(theme()->style(WidgetStyleTypes::FrameWidget));
        FrameWidgetStyle::State *widgetState = downed ? &style->states["downed"] : &style->states["normal"];
        m_framedPrimitive.setTexturePosition(widgetState->rect, widgetState->borders);
    });


}

void FramedWidget::draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const
{
    (void)(elapsed);

    std::shared_ptr<FrameWidgetStyle> style = std::static_pointer_cast<FrameWidgetStyle>(theme()->style(WidgetStyleTypes::FrameWidget));
    FrameWidgetStyle::State *widgetState = isMouseHover() ? &style->states["hovered"] : &style->states["normal"];
    if (!isEnabled())
        widgetState = &style->states["disabled"];

    state.transform *= transform();
    state.texture = widgetState->texture->source().get();
    m_framedPrimitive.draw(target, state);
}


}
