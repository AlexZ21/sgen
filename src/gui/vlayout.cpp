#include <sgen/gui/vlayout.h>

namespace sgen {

VLayout::VLayout()
{

}

void VLayout::updateContentGeometry()
{
    if (p_items.empty())
        return;

    int32_t contentsHeight = static_cast<int32_t>(p_size.y -  ((p_spacing * (p_items.size() - 1)) + p_maxSpacing));
    int32_t minimumHeight = minimumSize().y;
    contentsHeight = contentsHeight > minimumHeight ? contentsHeight : minimumHeight;
    int32_t contentWidth = std::max(minimumSize().x, p_size.x);
    int32_t nextPosition = p_position.y;

    for (std::shared_ptr<Item> &item : p_items) {
        if (item->type != Item::SPACING) {
            int32_t itemHeight = static_cast<int32_t>(contentsHeight * ((float)item->strechFactor / p_maxFactor));
            item->updateContentGeometry({p_position.x, nextPosition}, {contentWidth, itemHeight});
            nextPosition += (itemHeight + p_spacing);
        } else {
            std::shared_ptr<SpacingItem> spacingItem = std::static_pointer_cast<SpacingItem>(item);
            nextPosition += (spacingItem->space + p_spacing);
        }
    }
}

sf::Vector2i VLayout::minimumSize()
{
    int32_t minimumHeight = static_cast<int32_t>((p_spacing * (p_items.size() - 1)));
    int32_t minimumWidth = 0;
    for (std::shared_ptr<Item> &item : p_items) {
        minimumHeight += item->minimumSize().y;
        if (item->minimumSize().x > minimumWidth)
            minimumWidth = item->minimumSize().x;
    }
    return {minimumWidth, minimumHeight};
}

}
