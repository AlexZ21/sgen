#include <sgen/gui/widget.h>
#include <sgen/graphics/clipping.h>
#include <sgen/gui/layout.h>
#include <sgen/common/log.h>

#include <algorithm>

namespace sgen {

Widget::Widget() :
      m_needUpdateGlobalBounds(true),
      m_enabled(true),
      m_visible(true),
      m_mouseHover(false),
      m_ignoreMouseHover(false),
      m_mouseDown(false),
      m_focused(false),
      m_focusable(true),
      m_draggable(false),
      m_contentMargins({5, 5, 5, 5})
{
    setSize({100, 100});
    setMinimumSize(64, 64);
}

Widget::~Widget()
{
    setParent(nullptr);
}

bool Widget::event(const Event *event)
{
    if (event->type == EventTypes::Resized) {
        const ResizedEvent *e = static_cast<const ResizedEvent *>(event);
        setSize(static_cast<int32_t>(e->width), static_cast<int32_t>(e->height));
        return false;
    }

    if (event->type == EventTypes::MouseMoved) {
        return false;
    }

    return false;
}

void Widget::draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const
{
    (void)(target);
    (void)(state);
    (void)(elapsed);
}

sf::FloatRect Widget::localBounds() const
{
    return static_cast<sf::FloatRect>(sf::IntRect(sf::Vector2i(0, 0), size()));
}

sf::FloatRect Widget::globalBounds() const
{
    if (m_needUpdateGlobalBounds) {
        sf::Transform t = transform();
        std::shared_ptr<Widget> parent = m_parent.lock();
        while (parent) {
            t.translate(parent->m_contentMargins.at(0), parent->m_contentMargins.at(1));
            t *= parent->transform();
            parent = parent->m_parent.lock();
        }
        m_globalBounds = t.transformRect(localBounds());
        m_needUpdateGlobalBounds = false;
    }
    return m_globalBounds;
}

void Widget::setPosition(float x, float y)
{
    Drawable::setPosition(x, y);
    updateGlobalBounds();
}

void Widget::setPosition(const sf::Vector2f &position)
{
    Drawable::setPosition(position);
    updateGlobalBounds();
}

sf::Vector2i Widget::size() const
{
    return m_size;
}

void Widget::setSize(const sf::Vector2i &size)
{
    if (size.x <= 0 || size.y <= 0)
        return;

    if (m_size != size) {
        m_size = size;

        if (m_size.x < m_minimumSize.x)
            m_size.x = m_minimumSize.x;

        if (m_size.y < m_minimumSize.y)
            m_size.y = m_minimumSize.y;

        updateLayoutContentGeometry();
        updateGlobalBounds();
        resized(m_size.x, m_size.y);
    }
}

void Widget::setSize(int32_t width, int32_t height)
{
    setSize(sf::Vector2i(width, height));
}

sf::Vector2i Widget::minimumSize() const
{
    return m_minimumSize;
}

void Widget::setMinimumSize(const sf::Vector2i &minimumSize)
{
    if (minimumSize.x <= 0 || minimumSize.y <= 0)
        return;

    if (m_minimumSize != minimumSize) {
        m_minimumSize = minimumSize;
        if (m_minimumSize.x < m_size.x)
            setSize(m_minimumSize.x, m_size.y);

        if (m_minimumSize.y < m_size.y)
            setSize(m_size.x, m_minimumSize.y);
    }
}

void Widget::setMinimumSize(int32_t width, int32_t height)
{
    setMinimumSize(sf::Vector2i(width, height));
}

void Widget::setParent(const std::shared_ptr<Widget> &parent)
{
    auto p = m_parent.lock();
    if (p)
        p->removeChild(shared_from_this());

    if (parent)
        parent->addChild(shared_from_this());
}

void Widget::addChild(const std::shared_ptr<Widget> &child)
{
    if (child == nullptr)
        return;
    auto it = std::find(m_children.begin(), m_children.end(), child);
    if (it != m_children.end())
        return;
    m_children.push_back(child);
    child->m_parent = shared_from_this();
    child->m_theme = m_theme;
    child->updateGlobalBounds();
}

void Widget::removeChild(const std::shared_ptr<Widget> &child)
{
    if (child == nullptr)
        return;
    auto it = std::find(m_children.begin(), m_children.end(), child);
    if (it == m_children.end())
        return;
    m_children.erase(it);
    child->m_parent.reset();
    child->updateGlobalBounds();
}

std::shared_ptr<GuiTheme> Widget::theme() const
{
    return m_theme;
}

void Widget::setTheme(const std::shared_ptr<GuiTheme> &theme, bool updateChildren)
{
    m_theme = theme;
    themeChanged();

    if (updateChildren) {
        for (std::shared_ptr<Widget> &child : m_children) {
            child->setTheme(m_theme, updateChildren);
        }
    }
}

void Widget::drawWidget(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const
{
    if (!m_theme)
        return;

    draw(target, state, elapsed);

    state.transform *= transform();
    state.transform.translate(m_contentMargins.at(0), m_contentMargins.at(1));

    float clipWidth = m_size.x - (m_contentMargins.at(2) + m_contentMargins.at(0));
    float clipHeight = m_size.y - (m_contentMargins.at(1) + m_contentMargins.at(3));
    Clipping clipping(target, state, {0, 0}, {clipWidth, clipHeight});

    for (const std::shared_ptr<Widget> &child : m_children) {
        if (child->isVisible())
            child->drawWidget(target, state, elapsed);
    }
}

std::shared_ptr<Layout> Widget::layout() const
{
    return m_layout;
}

void Widget::setLayout(const std::shared_ptr<Layout> &layout)
{
    if (m_layout != layout) {
        m_layout = layout;
        updateLayoutContentGeometry();
    }
}

bool Widget::isIgnoreMouseHover() const
{
    return m_ignoreMouseHover;
}

void Widget::setIgnoreMouseHover(bool ignoreMouseHover)
{
    m_ignoreMouseHover = ignoreMouseHover;
}

bool Widget::isDraggable() const
{
    return m_draggable;
}

void Widget::setDraggable(bool draggable)
{
    m_draggable = draggable;
}

sf::Vector2i Widget::globalToLocal(const sf::Vector2i &point) const
{
    return globalToLocal(point.x, point.y);
}

sf::Vector2i Widget::globalToLocal(int32_t x, int32_t y) const
{
    sf::FloatRect bounds = globalBounds();
    return sf::Vector2i(x - static_cast<int32_t>(bounds.left), y - static_cast<int32_t>(bounds.top));
}

bool Widget::isChild(const std::shared_ptr<Widget> &widget) const
{
    for (const std::shared_ptr<Widget> &child : m_children) {
        if (child == widget)
            return true;
        if (child->isChild(widget))
            return true;
    }

    return false;
}

bool Widget::dropWidget(const sf::Vector2i &point, const sf::Vector2i &handlePoint, const std::shared_ptr<Widget> &widget)
{
    widget->setParent(shared_from_this());
    widget->setPosition(point.x - handlePoint.x,
                        point.y - handlePoint.y);
    return true;
}

bool Widget::isMouseHover() const
{
    return m_mouseHover;
}

bool Widget::isMouseDown() const
{
    return m_mouseDown;
}

void Widget::setMouseHover(bool mouseHover)
{
    if (m_mouseHover != mouseHover) {
        m_mouseHover = mouseHover;
        mouseHoverChanged(mouseHover);
    }
}

void Widget::setMouseDown(bool mouseDown)
{
    if (m_mouseDown != mouseDown) {
        m_mouseDown = mouseDown;
        mouseDownChanged(mouseDown);
    }
}

void Widget::updateGlobalBounds()
{
    m_needUpdateGlobalBounds = true;
    for (std::shared_ptr<Widget> &child : m_children)
        child->updateGlobalBounds();
}

void Widget::updateLayoutContentGeometry()
{
    if (m_layout) {
        m_layout->setSize({m_size.x - (m_contentMargins[0] + m_contentMargins[2]),
                           m_size.y - (m_contentMargins[1] + m_contentMargins[3])});
        m_layout->updateContentGeometry();
    }
}

bool Widget::isEnabled() const
{
    return m_enabled;
}

void Widget::setEnabled(bool enabled)
{
    m_enabled = enabled;
    if (m_mouseHover) {
        m_mouseHover = false;
        mouseHoverChanged(false);
    }
    for (std::shared_ptr<Widget> &child : m_children) {
        child->setEnabled(m_enabled);
    }
    enableChanged(m_enabled);
}

bool Widget::isVisible() const
{
    return m_visible;
}

void Widget::setVisible(bool visible)
{
    m_visible = visible;
    visibleChanged(m_visible);
}

std::vector<int32_t> Widget::contentMargins() const
{
    return m_contentMargins;
}

void Widget::setContentMargins(const std::vector<int32_t> &contentMargins)
{
    m_contentMargins = contentMargins;
    for (std::shared_ptr<Widget> &child : m_children)
        child->updateGlobalBounds();
}

}
