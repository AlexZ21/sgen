#include <sgen/graphics/animatedtexturestate.h>
#include <sgen/common/log.h>

namespace sgen {

AnimatedTextureState::AnimatedTextureState(const std::shared_ptr<AnimatedTexture> &texture) :
    m_texture(texture),
    m_elapsedTime(sf::milliseconds(0)),
    m_currentFrame(0),
    m_paused(true)
{
    m_frameTime = texture->frameTime();
    m_looped = texture->isLooped();
}

void AnimatedTextureState::play()
{
    m_paused = false;
    m_elapsedTime = sf::milliseconds(0);
}

void AnimatedTextureState::pause()
{
    m_looped = true;
}

void AnimatedTextureState::stop()
{
    m_looped = true;
    m_currentFrame = 0;
}

sf::IntRect AnimatedTextureState::rect() const
{
    return m_texture->frameRect(m_currentFrame);
}

void AnimatedTextureState::onUpdated(const std::function<void ()> &onUpdated)
{
    m_onUpdated = onUpdated;
}

void AnimatedTextureState::update(sf::Time elapsed)
{
    if (!m_paused) {
        m_elapsedTime += elapsed;

        uint64_t frames = static_cast<uint64_t>(m_elapsedTime / m_frameTime);
        if (frames > 0) {
            uint32_t oldFrame = m_currentFrame;
            m_currentFrame = static_cast<uint32_t>((m_currentFrame + frames) % m_texture->frameCount());
            m_elapsedTime = sf::milliseconds(0);
            if (!m_looped && m_currentFrame <= oldFrame)
                m_paused = true;
            if (m_onUpdated)
                m_onUpdated();
        }
    }
}

bool AnimatedTextureState::isLooped() const
{
    return m_looped;
}

void AnimatedTextureState::setLooped(bool looped)
{
    m_looped = looped;
}

sf::Time AnimatedTextureState::frameTime() const
{
    return m_frameTime;
}

void AnimatedTextureState::setFrameTime(const sf::Time &frameTime)
{
    m_frameTime = frameTime;
}

}
