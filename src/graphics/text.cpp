#include <sgen/graphics/text.h>
#include <sgen/common/log.h>

#include <limits>
#include <math.h>

namespace sgen {

const float EPS = std::numeric_limits<float>::epsilon();
inline bool compareFloats(const float &f1, const float &f2) {
    return (std::abs(f1 - f2) <= EPS);
}

// Add an underline or strikethrough line to the vertex array
void addLine(sf::VertexArray &vertices, float lineLength, float lineTop, const sf::Color &color,
             float offset, float thickness, float outlineThickness = 0)
{
    float top = std::floor(lineTop + offset - (thickness / 2) + 0.5f);
    float bottom = top + std::floor(thickness + 0.5f);

    vertices.append(sf::Vertex(
        sf::Vector2f(-outlineThickness, top - outlineThickness), color, sf::Vector2f(1, 1)));
    vertices.append(sf::Vertex(sf::Vector2f(lineLength + outlineThickness, top    - outlineThickness), color, sf::Vector2f(1, 1)));
    vertices.append(sf::Vertex(sf::Vector2f(-outlineThickness,             bottom + outlineThickness), color, sf::Vector2f(1, 1)));
    vertices.append(sf::Vertex(sf::Vector2f(-outlineThickness,             bottom + outlineThickness), color, sf::Vector2f(1, 1)));
    vertices.append(sf::Vertex(sf::Vector2f(lineLength + outlineThickness, top    - outlineThickness), color, sf::Vector2f(1, 1)));
    vertices.append(sf::Vertex(sf::Vector2f(lineLength + outlineThickness, bottom + outlineThickness), color, sf::Vector2f(1, 1)));
}

// Add a glyph quad to the vertex array
void addGlyphQuad(sf::VertexArray &vertices, sf::Vector2f position, const sf::Color &color,
                  const sf::Glyph &glyph, float italicShear, float outlineThickness = 0)
{
    float padding = 1.0;

    float left = glyph.bounds.left - padding;
    float top = glyph.bounds.top - padding;
    float right = glyph.bounds.left + glyph.bounds.width + padding;
    float bottom = glyph.bounds.top + glyph.bounds.height + padding;

    float u1 = static_cast<float>(glyph.textureRect.left) - padding;
    float v1 = static_cast<float>(glyph.textureRect.top) - padding;
    float u2 = static_cast<float>(glyph.textureRect.left + glyph.textureRect.width) + padding;
    float v2 = static_cast<float>(glyph.textureRect.top + glyph.textureRect.height) + padding;

    vertices.append(sf::Vertex(
        sf::Vector2f(
            position.x + left - italicShear * top - outlineThickness,
            position.y + top - outlineThickness),
        color,
        sf::Vector2f(u1, v1)));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + right - italicShear * top    - outlineThickness, position.y + top    - outlineThickness), color, sf::Vector2f(u2, v1)));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + left  - italicShear * bottom - outlineThickness, position.y + bottom - outlineThickness), color, sf::Vector2f(u1, v2)));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + left  - italicShear * bottom - outlineThickness, position.y + bottom - outlineThickness), color, sf::Vector2f(u1, v2)));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + right - italicShear * top    - outlineThickness, position.y + top    - outlineThickness), color, sf::Vector2f(u2, v1)));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + right - italicShear * bottom - outlineThickness, position.y + bottom - outlineThickness), color, sf::Vector2f(u2, v2)));
}

Text::Text()
{
}

Text::Text(const std::shared_ptr<Font> &font)
    : m_size({64, 64}),
      m_wordWrap(false),
      m_elided(false),
      m_characterSize(30),
      m_letterSpacingFactor(1.0f),
      m_lineSpacingFactor(1.0f),
      m_style(Regular),
      m_fillColor(255, 255, 255),
      m_outlineColor(0, 0, 0),
      m_outlineThickness(0),
      m_vertices(sf::Triangles),
      m_outlineVertices(sf::Triangles),
      m_bounds(),
      m_geometryNeedUpdate(true),
      m_fontResourceId()
{
    setFont(font);
}

Text::Text(const std::shared_ptr<Font> &font, const sf::String &string)
    : m_string(string),
      m_size({64, 64}),
      m_wordWrap(false),
      m_elided(false),
      m_characterSize(30),
      m_letterSpacingFactor(1.0f),
      m_lineSpacingFactor(1.0f),
      m_style(Regular),
      m_fillColor(255, 255, 255),
      m_outlineColor(0, 0, 0),
      m_outlineThickness(0),
      m_vertices(sf::Triangles),
      m_outlineVertices(sf::Triangles),
      m_bounds(),
      m_geometryNeedUpdate(true),
      m_fontResourceId()
{
    setFont(font);
}

void Text::setFont(const std::shared_ptr<Font> &font)
{
    if (font && m_font != font) {
        m_font = font;
        m_geometryNeedUpdate = true;
    }
}

std::shared_ptr<Font> Text::font() const
{
    return m_font;
}

sf::String Text::string() const
{
    return m_string;
}

void Text::setString(const sf::String &string)
{
    if (m_string != string) {
        m_string = string;
        m_geometryNeedUpdate = true;
    }
}

sf::Vector2i Text::size() const
{
    return m_size;
}

void Text::setSize(int32_t width, int32_t height)
{
    if (m_size.x == width && m_size.y == height)
        return;
    m_size.x = width;
    m_size.y = height;
    m_geometryNeedUpdate = true;
}

void Text::setSize(const sf::Vector2i &size)
{
    if (m_size != size) {
        m_size = size;
        m_geometryNeedUpdate = true;
    }
}

bool Text::isWordWrap() const
{
    return m_wordWrap;
}

void Text::setWordWrap(bool wordWrap)
{
    if (m_wordWrap != wordWrap)
    {
        m_wordWrap = wordWrap;
        m_geometryNeedUpdate = true;
    }
}

bool Text::isElided() const
{
    return m_elided;
}

void Text::setElided(bool elided)
{
    if (m_elided != elided)
    {
        m_elided = elided;
        m_geometryNeedUpdate = true;
    }
}

uint32_t Text::characterSize() const
{
    return m_characterSize;
}

void Text::setCharacterSize(const uint32_t &characterSize)
{
    if (m_characterSize != characterSize) {
        m_characterSize = characterSize;
        m_geometryNeedUpdate = true;
    }
}

float Text::letterSpacingFactor() const
{
    return m_letterSpacingFactor;
}

void Text::setLetterSpacingFactor(float letterSpacingFactor)
{
    if (!compareFloats(m_letterSpacingFactor, letterSpacingFactor)) {
        m_letterSpacingFactor = letterSpacingFactor;
        m_geometryNeedUpdate = true;
    }
}

float Text::lineSpacingFactor() const
{
    return m_lineSpacingFactor;
}

void Text::setLineSpacingFactor(float lineSpacingFactor)
{
    if (!compareFloats(m_lineSpacingFactor, lineSpacingFactor)) {
        m_lineSpacingFactor = lineSpacingFactor;
        m_geometryNeedUpdate = true;
    }
}

Text::Style Text::style() const
{
    return m_style;
}

void Text::setStyle(const Style &style)
{
    if (m_style != style) {
        m_style = style;
        m_geometryNeedUpdate = true;
    }
}

sf::Color Text::fillColor() const
{
    return m_fillColor;
}

void Text::setFillColor(const sf::Color &fillColor)
{
    if (m_fillColor != fillColor) {
        m_fillColor = fillColor;
        if (!m_geometryNeedUpdate) {
            for (std::size_t i = 0; i < m_vertices.getVertexCount(); ++i)
                m_vertices[i].color = m_fillColor;
        }
    }
}

sf::Color Text::outlineColor() const
{
    return m_outlineColor;
}

void Text::setOutlineColor(const sf::Color &outlineColor)
{
    if (m_outlineColor != outlineColor) {
        m_outlineColor = outlineColor;
        if (!m_geometryNeedUpdate) {
            for (std::size_t i = 0; i < m_outlineVertices.getVertexCount(); ++i)
                m_outlineVertices[i].color = m_outlineColor;
        }
    }
}

float Text::outlineThickness() const
{
    return m_outlineThickness;
}

void Text::setOutlineThickness(float outlineThickness)
{
    m_outlineThickness = outlineThickness;
}

sf::FloatRect Text::localBounds() const
{
    ensureGeometryUpdate();
    return m_bounds;
}

sf::FloatRect Text::globalBounds() const
{
    return transform().transformRect(localBounds());
}

void Text::draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed) const
{
    if (m_font) {
        ensureGeometryUpdate();

        states.transform *= transform();
        states.texture = &m_font->source()->getTexture(m_characterSize);

        if (!compareFloats(m_outlineThickness, 0))
            target.draw(m_outlineVertices, states);

        target.draw(m_vertices, states);
    }
}

void Text::ensureGeometryUpdate() const
{
    if (!m_font)
        return;

    m_font->resourceId();
    // Do nothing, if geometry has not changed and the font texture has not changed
    if (!m_geometryNeedUpdate && m_font->resourceId() == m_fontResourceId)
        return;

    // Save the current fonts texture id
    m_fontResourceId = m_font->resourceId();

    // Mark geometry as updated
    m_geometryNeedUpdate = false;

    // Clear the previous geometry
    m_vertices.clear();
    m_outlineVertices.clear();
    m_bounds = sf::FloatRect();

    // No text: nothing to draw
    if (m_string.isEmpty())
        return;

    // Compute values related to the text style
    bool isBold = m_style & Bold;
    bool isUnderlined = m_style & Underlined;
    bool isStrikeThrough = m_style & StrikeThrough;
    float italicShear = (m_style & Italic) ? 0.209f : 0.f; // 12 degrees in radians
    float underlineOffset = m_font->source()->getUnderlinePosition(m_characterSize);
    float underlineThickness = m_font->source()->getUnderlineThickness(m_characterSize);

    // Compute the location of the strike through dynamically
    // We use the center point of the lowercase 'x' glyph as the reference
    // We reuse the underline thickness as the thickness of the strike through as well
    sf::FloatRect xBounds = m_font->source()->getGlyph(L'x', m_characterSize, isBold).bounds;
    float strikeThroughOffset = xBounds.top + xBounds.height / 2.f;

    // Precompute the variables needed by the algorithm
    float whitespaceWidth = m_font->source()->getGlyph(L' ', m_characterSize, isBold).advance;
    float letterSpacing = ( whitespaceWidth / 3.f ) * ( m_letterSpacingFactor - 1.f );
    whitespaceWidth += letterSpacing;
    float lineSpacing = m_font->source()->getLineSpacing(m_characterSize) * m_lineSpacingFactor;
    float x = 0.0f;
    float y = static_cast<float>(m_characterSize);

    // Create one quad for each character
    float minX = static_cast<float>(m_characterSize);
    float minY = static_cast<float>(m_characterSize);
    float maxX = 0.0f;
    float maxY = 0.0f;

    int32_t currentStringPos = 0;
    int32_t startWordPos = -1;
    int32_t endWordPos = -1;
    float wordWidth = 0;
    bool addSpacingSymbol = false;
    uint32_t previousChar = 0;
    float xSpacing = 0.0f;
    float ySpacing = 0.0f;

    float elidedWidth = 0.0f;
    if (m_elided) {
        const sf::Glyph &glyph = m_font->source()->getGlyph('.', m_characterSize, isBold);
        elidedWidth = (glyph.advance + letterSpacing) * 3;
    }
    float rightTextBorder = m_size.x - (elidedWidth + whitespaceWidth);
    float bottomTextBorder = m_wordWrap ? m_size.y : lineSpacing;

    while (currentStringPos < m_string.getSize()) {
        uint32_t currentChar = m_string[currentStringPos];

        if ((currentChar != L' ') && (currentChar != L'\n') && (currentChar != L'\t')) {
            // Starting new word
            if (startWordPos == -1)
                startWordPos = currentStringPos;

            wordWidth += m_font->source()->getKerning(previousChar, currentChar, m_characterSize);
            wordWidth += m_font->source()->getGlyph(currentChar, m_characterSize, isBold).advance + letterSpacing;

        } else {
            // End word
            if (startWordPos != -1)
                endWordPos = currentStringPos;
            addSpacingSymbol = true;
        }

        if (currentStringPos == (m_string.getSize() - 1))
            endWordPos = m_string.getSize();

        if (startWordPos != -1 && endWordPos != -1) {
            minX = std::min(minX, x);
            minY = std::min(minY, y);

            if (m_wordWrap) {
                x += xSpacing;
                y += ySpacing;

                if (x + wordWidth > m_size.x && x != 0) {
                    y += lineSpacing;
                    if (y < bottomTextBorder)
                        x = 0;
                }

                if (y > bottomTextBorder) {
                    x -= xSpacing;
                }

                xSpacing = 0;
                ySpacing = 0;
            } else {
                x += xSpacing;
                y += ySpacing;
                xSpacing = 0;
                ySpacing = 0;
            }

            if (y < bottomTextBorder) {
                // Add word
                for (int32_t i = startWordPos; i < endWordPos; ++i) {
                    uint32_t subChar = m_string[i];
                    const sf::Glyph &glyph = m_font->source()->getGlyph(subChar, m_characterSize, isBold);

                    if (x + glyph.advance + letterSpacing > m_size.x) {
                        if (m_wordWrap) {
                            y += lineSpacing;
                            x = 0;
                        } else {
                            break;
                        }
                    }

                    if (isUnderlined) {
                        addLine(m_vertices, x, y, m_fillColor, underlineOffset, underlineThickness);
                        if (m_outlineThickness != 0)
                            addLine(m_outlineVertices,
                                    x,
                                    y,
                                    m_outlineColor,
                                    underlineOffset,
                                    underlineThickness,
                                    m_outlineThickness);
                    }

                    if (isStrikeThrough) {
                        addLine(m_vertices, x, y, m_fillColor, strikeThroughOffset, underlineThickness);
                        if (m_outlineThickness != 0)
                            addLine(m_outlineVertices,
                                    x,
                                    y,
                                    m_outlineColor,
                                    strikeThroughOffset,
                                    underlineThickness,
                                    m_outlineThickness);
                    }

                    if (m_outlineThickness != 0) {
                        const sf::Glyph &glyph = m_font->source()->getGlyph(subChar, m_characterSize, isBold, m_outlineThickness);

                        float left = glyph.bounds.left;
                        float top = glyph.bounds.top;
                        float right = glyph.bounds.left + glyph.bounds.width;
                        float bottom = glyph.bounds.top + glyph.bounds.height;

                        addGlyphQuad(m_outlineVertices,
                                     sf::Vector2f(x, y),
                                     m_outlineColor,
                                     glyph,
                                     italicShear,
                                     m_outlineThickness);

                        minX = std::min(minX, x + left - italicShear * bottom - m_outlineThickness);
                        maxX = std::max(maxX, x + right - italicShear * top - m_outlineThickness);
                        minY = std::min(minY, y + top - m_outlineThickness);
                        maxY = std::max(maxY, y + bottom - m_outlineThickness);
                    }

                    addGlyphQuad(m_vertices, sf::Vector2f(x, y), m_fillColor, glyph, italicShear);

                    if (m_outlineThickness == 0) {
                        float left = glyph.bounds.left;
                        float top = glyph.bounds.top;
                        float right = glyph.bounds.left + glyph.bounds.width;
                        float bottom = glyph.bounds.top + glyph.bounds.height;

                        minX = std::min(minX, x + left - italicShear * bottom);
                        maxX = std::max(maxX, x + right - italicShear * top);
                        minY = std::min(minY, y + top);
                        maxY = std::max(maxY, y + bottom);
                    }

                    x += glyph.advance + letterSpacing;

                    if (m_wordWrap) {
                        if (m_elided && (y + lineSpacing > bottomTextBorder) && x > rightTextBorder)
                            break;
                    } else {
                        if (m_elided && x > rightTextBorder) {
                            y += lineSpacing;
                            break;
                        }
                    }
                }
            }

            wordWidth = 0;
            startWordPos = -1;
            endWordPos = -1;
        }

        if (m_elided && y > bottomTextBorder && currentStringPos < m_string.getSize()) {
            minX = std::min(minX, x);
            minY = std::min(minY, y);

            const sf::Glyph &dotGlyph = m_font->source()->getGlyph('.', m_characterSize, isBold);
            for (int32_t j = 0; j < 3; ++j) {
                if (isUnderlined) {
                    addLine(m_vertices, x, y - lineSpacing, m_fillColor, underlineOffset, underlineThickness);
                    if (m_outlineThickness != 0)
                        addLine(m_outlineVertices,
                                x,
                                y - lineSpacing,
                                m_outlineColor,
                                underlineOffset,
                                underlineThickness,
                                m_outlineThickness);
                }

                if (isStrikeThrough) {
                    addLine(m_vertices, x, y - lineSpacing, m_fillColor, strikeThroughOffset, underlineThickness);
                    if (m_outlineThickness != 0)
                        addLine(m_outlineVertices,
                                x,
                                y - lineSpacing,
                                m_outlineColor,
                                strikeThroughOffset,
                                underlineThickness,
                                m_outlineThickness);
                }

                if (m_outlineThickness != 0) {
                    const sf::Glyph &glyph = m_font->source()->getGlyph('.', m_characterSize, isBold, m_outlineThickness);

                    float left = glyph.bounds.left;
                    float top = glyph.bounds.top;
                    float right = glyph.bounds.left + glyph.bounds.width;
                    float bottom = glyph.bounds.top + glyph.bounds.height;

                    addGlyphQuad(m_outlineVertices,
                                 sf::Vector2f(x, y - lineSpacing),
                                 m_outlineColor,
                                 glyph,
                                 italicShear,
                                 m_outlineThickness);

                    minX = std::min(minX, x + left - italicShear * bottom - m_outlineThickness);
                    maxX = std::max(maxX, x + right - italicShear * top - m_outlineThickness);
                    minY = std::min(minY, y + top - m_outlineThickness);
                    maxY = std::max(maxY, y + bottom - m_outlineThickness);
                }

                addGlyphQuad(m_vertices, sf::Vector2f(x, y - lineSpacing), m_fillColor, dotGlyph, italicShear);

                if (m_outlineThickness == 0) {
                    float left = dotGlyph.bounds.left;
                    float top = dotGlyph.bounds.top;
                    float right = dotGlyph.bounds.left + dotGlyph.bounds.width;
                    float bottom = dotGlyph.bounds.top + dotGlyph.bounds.height;

                    minX = std::min(minX, x + left - italicShear * bottom);
                    maxX = std::max(maxX, x + right - italicShear * top);
                    minY = std::min(minY, y + top);
                    maxY = std::max(maxY, y + bottom);
                }

                x += dotGlyph.advance + letterSpacing;
            }
            break;
        }


        if (addSpacingSymbol) {
            switch (currentChar) {
            case L' ':  xSpacing += whitespaceWidth; break;
            case L'\t': xSpacing += whitespaceWidth * 4; break;
            case L'\n': ySpacing += lineSpacing; xSpacing = -1 * x; break;
            }
            addSpacingSymbol = false;
        }

        ++currentStringPos;
    }

    m_bounds.left = minX;
    m_bounds.top = minY;
    m_bounds.width = maxX - minX;
    m_bounds.height = maxY - minY;
}



}
