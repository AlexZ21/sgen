#include <sgen/graphics/texturecache.h>
#include <sgen/graphics/statictexture.h>
#include <sgen/graphics/animatedtexture.h>
#include <sgen/common/log.h>

#include <fstream>

namespace sgen {

bool checkArrayElement(const nlohmann::json &element, uint64_t size)
{
    if (!element.is_array())
        return false;
    if (element.size() != size)
        return false;
    return true;
}

TextureCache::TextureCache()
{

}

bool TextureCache::load(const nlohmann::json &json)
{
    if (!json.is_object() && !json.is_array()) {
        LDEBUG(LOG_CLASS_METHOD,
               ": Invalid json!");
        return false;
    }

    try {
        if (json.is_object()) {
            TextureType textureType = json["texture_type"].get<TextureType>();

            if (textureType == STATIC_TEXTURE)
                return createStaticTexture(json);

            if (textureType == ANIMATED_TEXTURE)
                return createAnimatedTexture(json);

        } else {
            bool result = false;
            for (auto &element : json) {
                if (load(element))
                    result = true;
            }
            return result;
        }

    } catch (const std::exception &e) {
        LWARN("Error loading resource from json: ", e.what());
        return false;
    }

    LWARN(LOG_CLASS_METHOD,
          ": Unknow texture type!");
    return false;
}

bool TextureCache::createStaticTexture(const nlohmann::json &json)
{
    try {
        ResourceType type = json["resource_type"].get<ResourceType>();
        ResourceId id = json["resource_id"].get<ResourceId>();

        std::string sourcePath = json["path"].get<std::string>();

        std::shared_ptr<sf::Texture> sourceTexture = getSourceTexture(sourcePath);
        if (!sourceTexture)
            return false;

        sf::IntRect rect;

        auto it = json.find("rect");
        if (it != json.end()) {
            if (!checkArrayElement(*it, 4)) {
                LWARN(LOG_CLASS_METHOD,
                      ": Wrong rect size!");
                return false;
            }
            rect.left = (*it)[0].get<int32_t>();
            rect.top = (*it)[1].get<int32_t>();
            rect.width = (*it)[2].get<int32_t>();
            rect.height = (*it)[3].get<int32_t>();
        } else {
            rect = sf::IntRect(0, 0,
                               static_cast<int32_t>(sourceTexture->getSize().x),
                               static_cast<int32_t>(sourceTexture->getSize().y));
        }

        std::shared_ptr<StaticTexture> texture = std::make_shared<StaticTexture>(sourceTexture, rect);
        texture->setResourceId(id);
//        texture->setResourceType(type);

        return add(texture);

    } catch (const std::exception &e) {
        LWARN(LOG_CLASS_METHOD,
              ": Error loading resource from json: ", e.what());
        return false;
    }
}

bool TextureCache::createAnimatedTexture(const nlohmann::json &json)
{
    try {
        ResourceType type = json["resource_type"].get<ResourceType>();
        ResourceId id = json["resource_id"].get<ResourceId>();

        std::string sourcePath = json["path"].get<std::string>();

        std::shared_ptr<sf::Texture> sourceTexture = getSourceTexture(sourcePath);
        if (!sourceTexture)
            return false;

        bool loop = json["loop"].get<bool>();
        int32_t interval = json["interval_ms"].get<int32_t>();

        auto it = json.find("frames");
        if (it != json.end()) {
            if (!it->is_array()) {
                LWARN(LOG_CLASS_METHOD,
                      ": Wrong frame rects!");
                return false;
            }

            std::vector<sf::IntRect> frames;

            for (auto &element : (*it)) {
                if (!checkArrayElement(element, 4)) {
                    LWARN(LOG_CLASS_METHOD,
                          ": Wrong frame rects!");
                    return false;
                }

                sf::IntRect rect;
                rect.left = element[0].get<int32_t>();
                rect.top = element[1].get<int32_t>();
                rect.width = element[2].get<int32_t>();
                rect.height = element[3].get<int32_t>();

                frames.push_back(rect);
            }

            std::shared_ptr<AnimatedTexture> texture =
                std::make_shared<AnimatedTexture>(sourceTexture, loop, sf::milliseconds(interval), frames);
            texture->setResourceId(id);
//            texture->setResourceType(type);

            return add(texture);

        } else {
            auto frameSizeIt = json.find("frame_size");
            if (frameSizeIt == json.end()) {
                LWARN(LOG_CLASS_METHOD,
                      ": Frame size not found!");
                return false;
            }
            if (!checkArrayElement(*frameSizeIt, 2)) {
                LWARN(LOG_CLASS_METHOD,
                      ": Frame size is wrong!");
                return false;
            }

            sf::Vector2i frameSize = {(*frameSizeIt)[0].get<int32_t>(), (*frameSizeIt)[1].get<int32_t>()};

            auto frameCountsIt = json.find("frame_counts");
            if (frameCountsIt == json.end()) {
                LWARN(LOG_CLASS_METHOD,
                      ": Frame counts not found!");
                return false;
            }
            if (!checkArrayElement(*frameCountsIt, 2)) {
                LWARN(LOG_CLASS_METHOD,
                      ": Frame counts is wrong!");
                return false;
            }

            sf::Vector2i frameCounts = {(*frameCountsIt)[0].get<int32_t>(), (*frameCountsIt)[1].get<int32_t>()};

            sf::Vector2i frameStartPos;
            auto frameStartPosIt = json.find("frame_start_pos");
            if (frameCountsIt != json.end()) {
                if (!checkArrayElement(*frameStartPosIt, 2)) {
                    LWARN(LOG_CLASS_METHOD,
                          ": Frame start position is wrong!");
                    return false;
                }
                frameStartPos = {(*frameStartPosIt)[0].get<int32_t>(), (*frameStartPosIt)[1].get<int32_t>()};
            }

            std::shared_ptr<AnimatedTexture> texture =
                std::make_shared<AnimatedTexture>(sourceTexture, loop, sf::milliseconds(interval), frameSize, frameCounts, frameStartPos);
            texture->setResourceId(id);
//            texture->setResourceType(type);

            return add(texture);
        }

    } catch (const std::exception &e) {
        LWARN("Error loading resource from json: ", e.what());
        return false;
    }
}

std::shared_ptr<sf::Texture> TextureCache::getSourceTexture(const std::string &path)
{
    auto it = m_sourceCache.find(path);
    if (it != m_sourceCache.end()) {
        std::shared_ptr<sf::Texture> texture = it->second.lock();
        if (texture) {
            return texture;
        } else {
            m_sourceCache.erase(it);
        }
    }

    std::shared_ptr<sf::Texture> texture = std::make_shared<sf::Texture>();
    // Temp solution
    if (!texture->loadFromFile(path)) {
        LDEBUG(LOG_CLASS_METHOD,
               ": Failed load texture: ", path);
        return nullptr;
    }

    m_sourceCache.emplace(path, texture);

    return texture;
}

}
