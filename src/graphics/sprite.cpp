#include <sgen/graphics/sprite.h>
#include <sgen/common/log.h>

#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

namespace sgen {

Sprite::Sprite() :
    Drawable(),
    m_vertices(sf::TriangleStrip)
{
    m_vertices.resize(4);
}

Sprite::Sprite(const std::shared_ptr<Texture> &texture, bool updateSize) :
    Drawable(),
    m_vertices(sf::TriangleStrip)
{
    m_vertices.resize(4);
    setTexture(texture, updateSize);
}

sf::Color Sprite::color() const
{
    return m_color;
}

void Sprite::setColor(const sf::Color &color)
{
    m_color = color;
    m_vertices[0].color = m_color;
    m_vertices[1].color = m_color;
    m_vertices[2].color = m_color;
    m_vertices[3].color = m_color;
}

std::shared_ptr<Texture> Sprite::texture() const
{
    return m_texture;
}

void Sprite::setTexture(const std::shared_ptr<Texture> &texture, bool updateSize)
{
    if (!texture)
        return;

    m_texture = texture;

    if (m_texture->type() == Texture::Animated){
        m_animatedState.
                reset(new AnimatedTextureState(std::static_pointer_cast<AnimatedTexture>(m_texture)));
        m_animatedState->onUpdated(std::bind(&Sprite::updateTexCoords, this));
    } else {
        m_animatedState.reset();
    }

    updateTexCoords();

    if (updateSize)
        updatePositions();
}

sf::Vector2i Sprite::size() const
{
    return sf::Vector2i(m_vertices[2].position.x, m_vertices[1].position.y);
}

void Sprite::setSize(int32_t width, int32_t height)
{
    m_vertices[1].position = sf::Vector2f(0, height);
    m_vertices[2].position = sf::Vector2f(width, 0);
    m_vertices[3].position = sf::Vector2f(width, height);
}

void Sprite::setSize(const sf::Vector2i &size)
{
    m_vertices[1].position = sf::Vector2f(0, size.y);
    m_vertices[2].position = sf::Vector2f(size.x, 0);
    m_vertices[3].position = sf::Vector2f(size.x, size.y);
}

AnimatedTextureState *Sprite::animatedState() const
{
    return m_animatedState.get();
}

sf::FloatRect Sprite::localBounds() const
{
    return m_vertices.getBounds();
}

sf::FloatRect Sprite::globalBounds() const
{
    return transform().transformRect(localBounds());
}

void Sprite::draw(sf::RenderTarget &target, sf::RenderStates state, sf::Time elapsed) const
{
    (void)(elapsed);
    if (m_texture) {
        if (m_animatedState)
            m_animatedState->update(elapsed);
        state.transform *= transform();
        state.texture = m_texture->source().get();
        target.draw(m_vertices, state);
    }
}

void Sprite::updatePositions()
{
    if (m_texture) {
        m_vertices[0].position = sf::Vector2f(0, 0);
        m_vertices[1].position = sf::Vector2f(0, m_texture->rect().height);
        m_vertices[2].position = sf::Vector2f(m_texture->rect().width, 0);
        m_vertices[3].position = sf::Vector2f(m_texture->rect().width,
                                              m_texture->rect().height);
    }
}

void Sprite::updateTexCoords()
{
    if (m_texture) {
        sf::IntRect frameRect = m_animatedState ?
                    m_animatedState->rect() : m_texture->rect();

        float left = static_cast<float>(frameRect.left);
        float right = left + frameRect.width;
        float top = static_cast<float>(frameRect.top);
        float bottom = top + frameRect.height;

        m_vertices[0].texCoords = sf::Vector2f(left, top);
        m_vertices[1].texCoords = sf::Vector2f(left, bottom);
        m_vertices[2].texCoords = sf::Vector2f(right, top);
        m_vertices[3].texCoords = sf::Vector2f(right, bottom);
    }
}

}

