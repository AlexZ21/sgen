#include <sgen/graphics/drawable.h>

namespace sgen {

Drawable::Drawable() :
    m_zIndex(0)
{

}

float Drawable::zIndex() const
{
    return m_zIndex;
}

void Drawable::setZIndex(float zIndex)
{
    m_zIndex = zIndex;
}

}
