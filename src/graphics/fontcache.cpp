#include <sgen/graphics/fontcache.h>
#include <sgen/graphics/font.h>
#include <sgen/common/log.h>

namespace sgen {

FontCache::FontCache()
{

}

bool FontCache::load(const nlohmann::json &json)
{
    try {
        ResourceType type = json["resource_type"].get<ResourceType>();
        ResourceId id = json["resource_id"].get<ResourceId>();

        std::string sourcePath = json["path"].get<std::string>();

        std::shared_ptr<sf::Font> sourceFont = getSourceFont(sourcePath);
        if (!sourceFont)
            return false;

        std::shared_ptr<Font> font = std::make_shared<Font>(sourceFont);
        font->setResourceId(id);

        return add(font);

    } catch (const std::exception &e) {
        LWARN(LOG_CLASS_METHOD,
              ": Error loading resource from json: ", e.what());
        return false;
    }
}

std::shared_ptr<sf::Font> FontCache::getSourceFont(const std::string &path)
{
    auto it = m_fontCache.find(path);
    if (it != m_fontCache.end()) {
        std::shared_ptr<sf::Font> font = it->second.lock();
        if (font) {
            return font;
        } else {
            m_fontCache.erase(it);
        }
    }

    std::shared_ptr<sf::Font> font = std::make_shared<sf::Font>();
    // Temp solution
    if (!font->loadFromFile(path)) {
        LDEBUG(LOG_CLASS_METHOD,
               ": Failed load font: ", path);
        return nullptr;
    }

    m_fontCache.emplace(path, font);

    return font;
}

}
