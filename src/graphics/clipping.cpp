#include <sgen/graphics/clipping.h>

#include <math.h>

namespace sgen {

sf::View Clipping::m_originalView;

Clipping::Clipping(sf::RenderTarget &target, const sf::RenderStates &states,
                   sf::Vector2f topLeft, sf::Vector2f size) :
      m_target(target),
      m_oldView(target.getView())
{
    sf::Vector2f bottomRight = sf::Vector2f(states.transform.transformPoint(topLeft + size));
    topLeft = sf::Vector2f(states.transform.transformPoint(topLeft));

    sf::Vector2f viewTopLeft = topLeft;
    size = bottomRight - topLeft;

    topLeft.x -= m_originalView.getCenter().x - (m_originalView.getSize().x / 2.f);
    topLeft.y -= m_originalView.getCenter().y - (m_originalView.getSize().y / 2.f);
    bottomRight.x -= m_originalView.getCenter().x - (m_originalView.getSize().x / 2.f);
    bottomRight.y -= m_originalView.getCenter().y - (m_originalView.getSize().y / 2.f);

    topLeft.x *= m_originalView.getViewport().width / m_originalView.getSize().x;
    topLeft.y *= m_originalView.getViewport().height / m_originalView.getSize().y;
    size.x *= m_originalView.getViewport().width / m_originalView.getSize().x;
    size.y *= m_originalView.getViewport().height / m_originalView.getSize().y;

    topLeft.x += m_originalView.getViewport().left;
    topLeft.y += m_originalView.getViewport().top;

    if (topLeft.x < m_oldView.getViewport().left) {
        size.x -= m_oldView.getViewport().left - topLeft.x;
        viewTopLeft.x += (m_oldView.getViewport().left - topLeft.x) * (m_originalView.getSize().x / m_originalView.getViewport().width);
        topLeft.x = m_oldView.getViewport().left;
    }

    if (topLeft.y < m_oldView.getViewport().top)  {
        size.y -= m_oldView.getViewport().top - topLeft.y;
        viewTopLeft.y += (m_oldView.getViewport().top - topLeft.y) * (m_originalView.getSize().y / m_originalView.getViewport().height);
        topLeft.y = m_oldView.getViewport().top;
    }

    if (size.x > m_oldView.getViewport().left + m_oldView.getViewport().width - topLeft.x)
        size.x = m_oldView.getViewport().left + m_oldView.getViewport().width - topLeft.x;
    if (size.y > m_oldView.getViewport().top + m_oldView.getViewport().height - topLeft.y)
        size.y = m_oldView.getViewport().top + m_oldView.getViewport().height - topLeft.y;

    if ((size.x >= 0) && (size.y >= 0)) {
        sf::View view{{std::round(viewTopLeft.x),
                       std::round(viewTopLeft.y),
                       std::round(size.x * m_originalView.getSize().x / m_originalView.getViewport().width),
                       std::round(size.y * m_originalView.getSize().y / m_originalView.getViewport().height)}};

        view.setViewport({topLeft.x, topLeft.y, size.x, size.y});
        target.setView(view);
    } else {
        sf::View emptyView{{0, 0, 0, 0}};
        emptyView.setViewport({0, 0, 0, 0});
        target.setView(emptyView);
    }
}

Clipping::~Clipping()
{
    m_target.setView(m_oldView);
}

void Clipping::setOriginalView(const sf::View &view)
{
    m_originalView = view;
}

}
