#include <sgen/graphics/statictexture.h>

namespace sgen {

StaticTexture::StaticTexture(const std::shared_ptr<sf::Texture> &source,
                             const sf::IntRect &rect):
       m_source(source)
{
    if (m_source && (rect.width == 0 || rect.height == 0)) {
        m_rect.width = static_cast<int32_t>(m_source->getSize().x);
        m_rect.height = static_cast<int32_t>(m_source->getSize().y);
    } else {
        m_rect = rect;
    }
}

StaticTexture::~StaticTexture()
{
}

Texture::Type StaticTexture::type() const
{
    return Static;
}

std::shared_ptr<sf::Texture> StaticTexture::source() const
{
    return m_source;
}

sf::IntRect StaticTexture::rect() const
{
    return m_rect;
}

}
