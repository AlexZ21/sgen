#include <sgen/graphics/animatedtexture.h>

namespace sgen {

AnimatedTexture::AnimatedTexture(const std::shared_ptr<sf::Texture> &source,
                                 bool looped,
                                 const sf::Time &frameTime,
                                 const std::vector<sf::IntRect> frames) :
    m_source(source),
    m_frames(frames),
    m_looped(looped),
    m_frameTime(frameTime)
{

}

AnimatedTexture::AnimatedTexture(const std::shared_ptr<sf::Texture> &source,
                                 bool looped,
                                 const sf::Time &frameTime,
                                 const sf::Vector2i &frameSize,
                                 const sf::Vector2i &frameCounts,
                                 const sf::Vector2i &startPoint) :
    m_source(source),
    m_looped(looped),
    m_frameTime(frameTime)
{

    for (int32_t ix = 0; ix < frameCounts.x; ++ix) {
        for (int32_t iy = 0; iy < frameCounts.y; ++iy) {
            m_frames.push_back(sf::IntRect(startPoint.x + ix * frameSize.x,
                                           startPoint.y + iy * frameSize.y,
                                           frameSize.x, frameSize.y));
        }
    }

}

sf::IntRect AnimatedTexture::frameRect(size_t frame) const
{
    if (frame >= m_frames.size())
        return sf::IntRect();
    return m_frames.at(frame);
}

size_t AnimatedTexture::frameCount() const
{
    return m_frames.size();
}

sf::Time AnimatedTexture::frameTime() const
{
    return m_frameTime;
}

bool AnimatedTexture::isLooped() const
{
    return m_looped;
}

Texture::Type AnimatedTexture::type() const
{
    return Animated;
}

std::shared_ptr<sf::Texture> AnimatedTexture::source() const
{
    return m_source;
}

sf::IntRect AnimatedTexture::rect() const
{
    return !m_frames.empty() ? m_frames.at(0) : sf::IntRect();
}

}
