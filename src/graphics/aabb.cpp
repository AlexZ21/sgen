#include <sgen/graphics/aabb.h>

#include <algorithm>
#include <limits>

namespace sgen {

const float EPS = std::numeric_limits<float>::epsilon();
inline bool compareFloats(const float &f1, const float &f2) {
    return (std::abs(f1 - f2) <= EPS);
}

AABB::AABB() :
      minX(0),
      minY(0),
      maxX(0),
      maxY(0)
{

}

AABB::AABB(float t_minX, float t_minY, float t_maxX, float t_maxY) :
      minX(t_minX),
      minY(t_minY),
      maxX(t_maxX),
      maxY(t_maxY)
{

}

AABB::AABB(const AABB &other) :
      minX(other.minX),
      minY(other.minY),
      maxX(other.maxX),
      maxY(other.maxY)
{

}

AABB::AABB(const sf::FloatRect &rect) :
      minX(rect.left),
      minY(rect.top),
      maxX(rect.left + rect.width),
      maxY(rect.top + rect.height)
{

}

AABB AABB::operator=(const AABB &other)
{
    minX = other.minX;
    minY = other.minY;
    maxX = other.maxX;
    maxY = other.maxY;
    return *this;
}

AABB AABB::operator=(const sf::FloatRect &rect)
{
    minX = rect.left;
    minY = rect.top;
    maxX = rect.left + rect.width;
    maxY = rect.top + rect.height;
    return *this;
}

bool AABB::operator==(const AABB &other)
{
    return compareFloats(minX, other.minX) && compareFloats(minY, other.minY) &&
        compareFloats(maxX, other.maxX) && compareFloats(maxY, other.maxY);
}

bool AABB::operator==(const sf::FloatRect &rect)
{
    return compareFloats(minX, rect.left) && compareFloats(minY, rect.top) &&
        compareFloats(maxX, rect.left + rect.width) && compareFloats(maxY, rect.top + rect.height);
}

bool AABB::isValid() const
{
    return (maxX > minX) && (maxY > minY);
}

float AABB::width() const
{
    return maxX - minX;
}

float AABB::height() const
{
    return maxY - minY;
}

float AABB::area() const
{
    return 2.0f * (width() * height());
}

bool AABB::overlaps(const AABB &other) const
{
    return (maxX > other.minX) && (minX < other.maxX) &&
        (maxY > other.minY) && (minY < other.maxY);
}

bool AABB::overlaps(const sf::FloatRect &rect) const
{
    return (maxX > rect.left) && (minX < (rect.left + rect.width)) &&
        (maxY > rect.top) && (minY < (rect.top + rect.height));
}

bool AABB::contains(const AABB &other) const
{
    return (other.minX >= minX) && (other.maxX <= maxX) &&
        (other.minY >= minY) && (other.maxY <= maxY);
}

bool AABB::contains(const sf::FloatRect &rect) const
{
    return (rect.left >= minX) && ((rect.left + rect.width) <= maxX) &&
        (rect.top >= minY) && ((rect.top + rect.height) <= maxY);
}

AABB AABB::merge(const AABB &other) const
{
    return AABB(std::min(minX, other.minX), std::min(minY, other.minY),
                std::max(maxX, other.maxX), std::max(maxY, other.maxY));
}

AABB AABB::merge(const sf::FloatRect &rect) const
{
    return AABB(std::min(minX, rect.left), std::min(minY, rect.top),
                std::max(maxX, (rect.left + rect.width)), std::max(maxY, (rect.top + rect.height)));
}

void AABB::merge(const AABB &other1, const AABB &other2)
{
    minX = std::min(other1.minX, other2.minX);
    minY = std::min(other1.minY, other2.minY);
    maxX = std::max(other1.maxX, other2.maxX);
    maxY = std::max(other1.maxY, other2.maxY);
}

AABB AABB::intersection(const AABB &other) const
{
    return AABB(std::max(minX, other.minX), std::max(minY, other.minY),
                std::min(maxX, other.maxX), std::min(maxY, other.maxY));
}

AABB AABB::intersection(const sf::FloatRect &rect) const
{
    return AABB(std::max(minX, rect.left), std::max(minY, rect.top),
                std::min(maxX, (rect.left + rect.width)), std::min(maxY, (rect.top + rect.height)));
}

void AABB::fatten(float extension)
{
    minX -= extension;
    minY -= extension;
    maxX += extension;
    maxY += extension;
}


}
