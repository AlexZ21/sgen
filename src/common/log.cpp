#include <sgen/common/log.h>

namespace sgen {

Log &Log::instance()
{
    static Log log;
    return log;
}

}
