#include <sgen/components/scenecomponent.h>

namespace sgen {

SceneComponent::SceneComponent()
{

}

Id SceneComponent::treeId() const
{
    return m_treeId;
}

void SceneComponent::setTreeId(const Id &treeId)
{
    m_treeId = treeId;
}

std::shared_ptr<Drawable> SceneComponent::drawable() const
{
    return m_drawable;
}

void SceneComponent::setDrawable(const std::shared_ptr<Drawable> &drawable)
{
    if (!drawable)
        return;
    m_drawable = drawable;

}

sf::FloatRect SceneComponent::globalBounds() const
{
    return m_globalBounds;
}

bool SceneComponent::isChanged()
{
    bool res = m_globalBounds == m_drawable->globalBounds();
    if (!res)
       m_globalBounds = m_drawable->globalBounds();
    return !res;
}

}
