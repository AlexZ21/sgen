#include <sgen/core/eventmanager.h>
#include <sgen/common/log.h>

namespace sgen {

EventManager::EventManager() :
      m_needUpdateEventHandlers(false)
{
}

void EventManager::sendEvent(Event *event)
{
    if (!event)
        return;
    m_events.push({nullptr, std::unique_ptr<Event>(event)});
}

void EventManager::sendEvent(EventHandler *receiver, Event *event)
{
    if (!event || !receiver)
        return;
    m_events.push({receiver, std::unique_ptr<Event>(event)});
}

void EventManager::addEventHandler(EventHandler *eventHandler)
{
    if (!eventHandler)
        return;
    m_addEventHandlers.push_back(eventHandler);
    m_needUpdateEventHandlers = true;
}

void EventManager::removeEventHandler(EventHandler *eventHandler)
{
    if (!eventHandler)
        return;
    m_removeEventHandlers.push_back(eventHandler);
    m_needUpdateEventHandlers = true;
}

void EventManager::processEvents()
{
    // Update event handlers
    if (m_needUpdateEventHandlers) {
        addEventHandlers();
        removeEventHandlers();

        // Sort event handlers after changes
        for (auto it : m_eventHandlers) {
            std::sort(it.second.begin(), it.second.end(),
                      [](EventHandler *a, EventHandler *b){
                          return a->eventPriority() > b->eventPriority();
                      });
        }

        m_needUpdateEventHandlers = false;
    }

    while (!m_events.empty()) {
        EventInfo &eventInfo = m_events.front();

        auto it = m_eventHandlers.find(eventInfo.event->type);
        if (it == m_eventHandlers.end()) {
            m_events.pop();
            continue;
        }

        if (eventInfo.receiver) {
            auto it2 = std::find(it->second.begin(), it->second.end(), eventInfo.receiver);
            if (it2 != it->second.end() && !(*it2)->ignoreEvent())
                (*it2)->event(eventInfo.event.get());
        } else {
            for (EventHandler *eventHandler : it->second) {
                if (!eventHandler->ignoreEvent()) {
                    if (eventHandler->event(eventInfo.event.get()))
                        break;
                }
            }
        }

        m_events.pop();
    }
}

void EventManager::addEventHandlers()
{
    for (EventHandler *eventHandler : m_addEventHandlers) {
        std::vector<int32_t> eventFilter = eventHandler->eventFilter();

        if (eventFilter.empty())
            eventFilter.push_back(-1);

        for (int32_t eventType : eventFilter) {
            auto it = m_eventHandlers.find(eventType);
            if (it == m_eventHandlers.end())
                m_eventHandlers.emplace(eventType, std::vector<EventHandler *>());

            std::vector<EventHandler *> &eventHandlers = m_eventHandlers[eventType];
            eventHandlers.push_back(eventHandler);
        }
    }

    m_addEventHandlers.clear();
}

void EventManager::removeEventHandlers()
{
    for (EventHandler *eventHandler : m_removeEventHandlers) {
        std::vector<int32_t> eventFilter = eventHandler->eventFilter();

        if (eventFilter.empty())
            eventFilter.push_back(-1);

        for (int32_t eventType : eventFilter) {
            auto it = m_eventHandlers.find(eventType);
            if (it == m_eventHandlers.end())
                continue;

            auto it2 = std::find(it->second.begin(), it->second.end(), eventHandler);
            if (it2 == it->second.end())
                continue;

            it->second.erase(it2);

            if (it->second.empty())
                m_eventHandlers.erase(it);
        }
    }

    m_removeEventHandlers.clear();
}

}
