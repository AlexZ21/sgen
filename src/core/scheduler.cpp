#include <sgen/core/scheduler.h>
#include <sgen/common/log.h>

#include <algorithm>

namespace sgen {

Scheduler::Scheduler()
{

}

Id Scheduler::createTimer(sf::Time interval, bool singleshot,
                          const std::function<void ()> &callback)
{
    Id id = m_items.create();
    Item *item = m_items[id];
    item->interval = interval;
    item->singleshot = singleshot;
    item->callback = callback;
    item->state = Item::INACTIVE;
    return id;
}

bool Scheduler::removeTimer(Id timerId)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    item->state = Item::WILL_BE_REMOVED;
    return true;
}

sf::Time Scheduler::timerInerval(Id timerId) const
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return sf::milliseconds(0);
    }
    const Item *item = m_items.at(timerId);
    return item->interval;
}

bool Scheduler::setTimerInterval(Id timerId, sf::Time interval)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    item->interval = interval;

    if (item->state == Item::ACTIVE)
        item->start = m_clock.getElapsedTime();

    return true;
}

bool Scheduler::isTimerSingleShot(Id timerId) const
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    const Item *item = m_items.at(timerId);
    return item->singleshot;
}

bool Scheduler::setTimerSingleShot(Id timerId, bool singleshot)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    item->singleshot = singleshot;

    if (item->state == Item::ACTIVE)
        item->start = m_clock.getElapsedTime();

    return true;
}

bool Scheduler::setTimerCallback(Id timerId, const std::function<void ()> &callback)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    item->callback = callback;

    if (item->state == Item::ACTIVE)
        item->start = m_clock.getElapsedTime();

    return true;
}

bool Scheduler::startTimer(Id timerId)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];

    if (item->state != Item::ACTIVE) {
        item->state = Item::ACTIVE;
        item->start = m_clock.getElapsedTime();
        m_ids.push_back(timerId);
    }

    return true;
}

bool Scheduler::stopTimer(Id timerId)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    if (item->state == Item::ACTIVE) {
        item->state = Item::INACTIVE;
        std::remove(m_ids.begin(), m_ids.end(), timerId);
    }
    return true;
}

bool Scheduler::isTimerActive(Id timerId)
{
    if (!m_items.valid(timerId)) {
        LWARN(LOG_CLASS_METHOD,
              ": Timer <", timerId.index() , "> not found!");
        return false;
    }
    Item *item = m_items[timerId];
    return item->state == Item::ACTIVE;
}

void Scheduler::update(sf::Time elapsed)
{
    (void)(elapsed);

    sf::Time e = m_clock.getElapsedTime();

    for (auto it = m_ids.begin(); it != m_ids.end(); /*++it*/) {
        Item *item = m_items[*it];

        switch (item->state) {
        case Item::WILL_BE_REMOVED:
            m_items.remove(*it);
            m_ids.erase(it);
            break;
        case Item::ACTIVE:
            if (item->start + item->interval < e) {
                if (item->callback)
                    item->callback();
                if (item->singleshot) {
                    item->state = Item::INACTIVE;
                    m_ids.erase(it);
                } else {
                    item->start = m_clock.getElapsedTime();
                    ++it;
                }
            } else {
                ++it;
            }
            break;
        default:
            ++it;
            break;
        }
    }
}

}
