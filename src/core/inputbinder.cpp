#include <sgen/core/inputbinder.h>
#include <sgen/common/log.h>

#include <bitset>
#include <algorithm>
#include <limits>

namespace sgen {

template<typename ContainerT, typename PredicateT>
void erase_if(ContainerT &items, const PredicateT &predicate) {
    for (auto it = items.begin(); it != items.end(); ) {
        if (predicate(*it)) it = items.erase(it);
        else ++it;
    }
}

InputBinder::InputBinder() :
      EventHandler({EventTypes::KeyPressed,
                    EventTypes::KeyReleased,
                    EventTypes::MouseButtonPressed,
                    EventTypes::MouseButtonReleased,
                    EventTypes::JoystickButtonPressed,
                    EventTypes::JoystickButtonReleased},
                   std::numeric_limits<int32_t>::max() - 1)

{
    setIgnoreEvent(true);
}

void InputBinder::addAction(const std::string &name, const std::function<void ()> &callback)
{
    if (!callback) {
        LERR("Action callback is null");
        return;
    }

    if (m_actions.find(name) != m_actions.end()) {
        LERR("Action <", name, "> already exists");
        return;
    }

    m_actions.emplace(name, callback);
}

void InputBinder::removeAction(const std::string &name)
{
    auto it = m_actions.find(name);
    if (it == m_actions.end())
        return;

    m_actions.erase(it);
}

void InputBinder::clearActions()
{
    m_actions.clear();
}

void InputBinder::bind(const std::string &actionName, InputBinder::InputType inputType,
                       sf::Keyboard::Key code, InputBinder::ModifierKeys modifiers)
{
    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Keyboard);
    hash ^= code << 1;
    hash ^= (bool)(modifiers & Alt) << 2;
    hash ^= (bool)(modifiers & Control) << 3;
    hash ^= (bool)(modifiers & Shift) << 4;
    hash ^= (bool)(modifiers & System) << 5;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void InputBinder::bind(const std::string &actionName, InputBinder::InputType inputType,
                       sf::Mouse::Button button)
{
    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Mouse);
    hash ^= button << 1;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void InputBinder::bind(const std::string &actionName, InputBinder::InputType inputType,
                       uint32_t joystickId, uint32_t button)
{
    size_t hash = 0;
    hash = std::hash<InputDeviceType>()(Mouse);
    hash ^= joystickId << 1;
    hash ^= button << 2;

    ShortcutState st;
    st.actionName = actionName;
    st.activated = false;
    st.inputType = inputType;

    m_shortcuts.emplace(hash, st);
}

void InputBinder::unbind(const std::string &actionName)
{
    erase_if(m_shortcuts, [actionName](const auto &it){ return it.second.actionName == actionName; });
}

void InputBinder::clearBinds()
{
    m_shortcuts.clear();
}

bool InputBinder::event(const Event *event)
{
    uint64_t hash = 0;

    if (event->type == EventTypes::KeyPressed ||
        event->type == EventTypes::KeyReleased) {
        const KeyEvent *e = static_cast<const KeyEvent*>(event);
        hash = std::hash<InputDeviceType>()(Keyboard);
        hash ^= e->code << 1;
        hash ^= e->alt << 2;
        hash ^= e->control << 3;
        hash ^= e->shift << 4;
        hash ^= e->system << 5;

    } else if (event->type == EventTypes::MouseButtonPressed ||
             event->type == EventTypes::MouseButtonReleased) {
        const MouseButtonEvent *e = static_cast<const MouseButtonEvent*>(event);
        hash = std::hash<InputDeviceType>()(Mouse);
        hash ^= e->button << 1;

    } else if (event->type == sf::Event::JoystickButtonPressed ||
             event->type == sf::Event::JoystickButtonReleased) {
        const JoystickButtonEvent *e = static_cast<const JoystickButtonEvent*>(event);
        hash = std::hash<InputDeviceType>()(Joystick);
        hash ^= e->joystickId << 1;
        hash ^= e->button << 2;

    }

    auto range = m_shortcuts.equal_range(hash);

    for (auto it = range.first; it != range.second; ++it) {
        ShortcutState &st = it->second;

        if (event->type == EventTypes::KeyPressed ||
            event->type == EventTypes::MouseButtonPressed ||
            event->type == EventTypes::JoystickButtonPressed) {
            if (st.inputType == PressOnce && !st.activated) {
                m_actions[st.actionName]();
                st.activated = true;
            } else if (st.inputType == Hold) {
                m_actions[st.actionName]();
            } else if (st.inputType == ReleaseOnce && st.activated) {
                st.activated = false;
            }

        } else {
            if (st.inputType == ReleaseOnce && !st.activated) {
                m_actions[st.actionName]();
                st.activated = true;
            } else if (st.inputType == PressOnce && st.activated) {
                st.activated = false;
            }

        }

    }

    return true;
}

}
