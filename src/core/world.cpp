#include <sgen/core/world.h>
#include <sgen/common/log.h>

#include <algorithm>

namespace sgen {

template <class T>
inline void ensureCapacity(T &container, typename T::size_type index) {
    if(container.size() <= index)
        container.resize(index + 1);
}

World::World()
{
}

void World::removeAllSystems()
{
    m_systems.clear();
}

const std::vector<std::unique_ptr<System> > &World::systems() const
{
    return m_systems;
}

Entity World::createEntity()
{
    m_aliveEntities.emplace_back(this, m_entityStates.create());
    return m_aliveEntities.back();
}

std::vector<Entity> World::createEntities(size_t amount)
{
    std::vector<Entity> temp;
    temp.reserve(amount);

    for(size_t i = 0; i < amount; ++i) {
        Entity e{this, m_entityStates.create()};
        m_aliveEntities.push_back(e);
        temp.push_back(e);
    }

    return temp;
}

Entity World::entity(size_t index)
{
    return Entity{this, m_entityStates.get(index)};
}

const std::vector<Entity> &World::entities() const
{
    return m_aliveEntities;
}

void World::clearEntitiesCache()
{
    m_aliveEntities.clear();
    clearTempEntitiesCache();
}

void World::clearTempEntitiesCache()
{
    m_killedEntities.clear();
    m_enabledEntities.clear();
    m_disabledEntities.clear();
}

void World::refresh()
{
    for(Entity &entity : m_enabledEntities) {
        EntityState *es = m_entityStates[entity.id()];
        es->enabled = true;

        for (auto &system : m_systems) {
            if (system->filter().doesPassFilter(componentMask(entity))) {
                if(es->systems.size() <= system->typeId() || !es->systems[system->typeId()]) {
                    system->add(entity);
                    ensureCapacity(es->systems, system->typeId());
                    es->systems[system->typeId()] = true;
                }
            } else if (es->systems.size() > system->typeId() &&
                     es->systems[system->typeId()]) {
                system->remove(entity);
                es->systems[system->typeId()] = false;
            }
        }
    }

    for(Entity &entity : m_disabledEntities) {
        EntityState *es = m_entityStates[entity.id()];
        es->enabled = false;

        for(auto &system : m_systems) {
            if(es->systems.size() <= system->typeId())
                continue;

            if(es->systems[system->typeId()]) {
                system->remove(entity);
                es->systems[system->typeId()] = false;
            }
        }
    }

    for(Entity &entity : m_killedEntities) {
        m_aliveEntities.erase(std::remove(m_aliveEntities.begin(),
                                          m_aliveEntities.end(),
                                          entity), m_aliveEntities.end());

        removeAllComponents(entity);
        m_entityStates.remove(entity.id());
    }

    clearTempEntitiesCache();

}

void World::clear()
{
    removeAllSystems();
    clearEntitiesCache();
    m_entityStates.clear();
}

void World::destroyEntity(const Entity &entity)
{
    disableEntity(entity);
    m_killedEntities.push_back(entity);
}

void World::destroyEntities(const std::vector<Entity> &entities)
{
    for(const Entity &entity : entities)
        destroyEntity(entity);
}

void World::enableEntity(const Entity &entity)
{
    if (!isValid(entity))
        return;
    m_enabledEntities.push_back(entity);
    refresh();
}

void World::disableEntity(const Entity &entity)
{
    if (!isValid(entity))
        return;
    m_disabledEntities.push_back(entity);
    refresh();
}

bool World::isEnabled(const Entity &entity) const
{
    return isValid(entity) ? m_entityStates.at(entity.id())->enabled : false;
}

bool World::isValid(const Entity &entity) const
{
    return m_entityStates.valid(entity.id());
}

bool World::addComponent(Entity &entity, Component *component, TypeId componentTypeId)
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return false;
    }

    bool oldState = isValid(entity);
    disableEntity(entity);

    EntityState *es = m_entityStates[entity.id()];
    es->components[componentTypeId].reset(component);
    es->mask[componentTypeId] = true;

    if (oldState)
        enableEntity(entity);

    return true;
}

bool World::removeComponent(Entity &entity, TypeId componentTypeId)
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return false;
    }

    bool oldState = isValid(entity);
    disableEntity(entity);

    EntityState *es = m_entityStates[entity.id()];
    es->components[componentTypeId].reset();
    es->mask[componentTypeId] = false;

    if (oldState)
        enableEntity(entity);

    return true;
}

void World::removeAllComponents(Entity &entity)
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return;
    }

    bool oldState = isValid(entity);
    disableEntity(entity);

    EntityState *es = m_entityStates[entity.id()];

    for(auto &c : es->components)
        c.reset();
    es->mask.reset();

    if (oldState)
        enableEntity(entity);
}

Component *World::component(const Entity &entity, TypeId componentTypeId) const
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return nullptr;
    }

    const EntityState *es = m_entityStates.at(entity.id());
    return es->components.at(componentTypeId).get();
}

ComponentMask World::componentMask(const Entity &entity) const
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return ComponentMask();
    }
    return m_entityStates.at(entity.id())->mask;
}

std::vector<Component *> World::components(const Entity &entity) const
{
    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return std::vector<Component *>();
    }

    const EntityState *es = m_entityStates.at(entity.id());
    std::vector<Component *> temp;
    temp.reserve(es->components.size());

    for(auto &i : es->components)
        temp.emplace_back(i.get());

    return temp;
}

bool World::hasComponent(const Entity &entity, TypeId componentTypeId) const
{

    if (!entity.isValid()) {
        LWARN(LOG_CLASS_METHOD,
              ": Invalid entity!");
        return false;
    }

    const EntityState *es = m_entityStates.at(entity.id());
    return es->components.at(componentTypeId) != nullptr;
}

}
