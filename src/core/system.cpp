#include <sgen/core/system.h>

#include <algorithm>

namespace sgen {

System::System(const Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK> &filter) :
    m_world(nullptr),
    m_filter(filter)
{

}

World *System::world() const
{
    return m_world;
}

const Filter<Component, MAX_AMOUNT_OF_COMPONENT_MASK> &System::filter() const
{
    return m_filter;
}

const std::vector<Entity> &System::entities() const
{
    return m_entities;
}

void System::add(const Entity &entity)
{
    m_entities.push_back(entity);
    onEntityAdded(entity);
}

void System::remove(const Entity &entity)
{
    m_entities.erase(std::remove(m_entities.begin(), m_entities.end(), entity), m_entities.end());
    onEntityRemoved(entity);
}

void System::setWorld(World *world)
{
    m_world = world;
    initialize();
}

}
