#include <sgen/core/resource.h>

namespace sgen {

Resource::Resource() :
      m_id(INVALID_RESOURCE_ID)
{

}

Resource::Resource(ResourceId t_id) :
      m_id(t_id)
{

}

ResourceId Resource::resourceId() const
{
    return m_id;
}

void Resource::setResourceId(ResourceId t_id)
{
    m_id = t_id;
}

}
