#include <sgen/core/resourcecache.h>
#include <sgen/common/log.h>

#include <fstream>

namespace sgen {

IResourceCache::IResourceCache()
{

}

IResourceCache::IResourceCache(const ResourceTypeList &resourceTypes) :
      m_resourceTypes(resourceTypes)
{

}

ResourceTypeList IResourceCache::resourceTypes() const
{
    return m_resourceTypes;
}

bool IResourceCache::load(const std::string &filePath)
{
    nlohmann::json json;

    try {
        std::ifstream ifs(filePath);
        ifs >> json;
        return load(json);

    } catch (const std::exception &e) {
        LWARN("Error load theme json: ", e.what());
    }

    return false;
}

bool IResourceCache::remove(ResourceId id)
{
    auto it = m_resources.find(id);
    if (it == m_resources.end()) {
        LWARN(LOG_CLASS_METHOD,
              ": Resource <", id, "> not found!");
        return false;
    }

    m_resources.erase(it);

    return true;
}

std::shared_ptr<Resource> IResourceCache::get(ResourceId id) const
{
    auto it = m_resources.find(id);
    if (it == m_resources.end()) {
        LWARN(LOG_CLASS_METHOD,
              ": Resource <", id, "> not found!");
        return nullptr;
    }
    return it->second;
}

bool IResourceCache::exist(ResourceId id) const
{
    auto it = m_resources.find(id);
    return it != m_resources.end();
}

}
