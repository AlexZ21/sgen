#include <sgen/core/game.h>
#include <sgen/core/eventmanager.h>
#include <sgen/core/inputbinder.h>
#include <sgen/core/resourcemanager.h>
#include <sgen/core/scheduler.h>
#include <sgen/core/scene.h>
#include <sgen/gui/guimanager.h>

#include <sgen/graphics/texturecache.h>
#include <sgen/graphics/fontcache.h>
#include <sgen/gui/guithemecache.h>

#include <sgen/common/log.h>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Sprite.hpp>

namespace sgen {

Game *Game::m_instance = nullptr;

Game *Game::instance()
{
    if (m_instance) {
        return m_instance;
    } else {
        static Game game;
        m_instance = &game;
        game.init();
        return m_instance;
    }
}

bool Game::showWindow(uint32_t width, uint32_t height, std::string title, bool fullscreen)
{
    if (m_window)
        return false;

    m_width = width;
    m_height = height;
    m_title = title;
    m_fullscreen = fullscreen;

    m_window = std::make_shared<sf::RenderWindow>(sf::VideoMode(m_width, m_height),
                                                  m_title,
                                                  m_fullscreen ? sf::Style::Fullscreen :
                                                               sf::Style::Default);
    //    m_window->setFramerateLimit(60);

    return true;
}

bool Game::closeWindow()
{
    if (!m_window)
        return false;

    m_window.reset();
    return true;
}

int Game::exec()
{
    mainLoop();
    return 0;
}

void Game::exit()
{
    m_running.store(false);
}

Game::Game() :
      m_tickTime(sf::milliseconds(16)),
      m_running(false)
{
}

Game::~Game()
{
    m_scene.reset();

    if (m_inputBinder)
        m_inputBinder.reset();

    m_eventManager.reset();

    if (m_window)
        m_window.reset();
}

void Game::init()
{
    LDEBUG("Init game");

    // Create managers
    m_eventManager = std::make_unique<EventManager>();
    m_scheduler = std::make_unique<Scheduler>();
    m_resourceManager = std::make_unique<ResourceManager>();
    m_guiManager = std::make_unique<GuiManager>();
    m_inputBinder = std::make_shared<InputBinder>();
    m_inputBinder->setIgnoreEvent(false);

    // Add caches
    m_resourceManager->addCache<TextureCache>();
    m_resourceManager->addCache<FontCache>();
    m_resourceManager->addCache<GuiThemeCache>();

}

void Game::mainLoop()
{
    m_running.store(true);

    sf::Clock loopClock;
    sf::Time elapsed = sf::milliseconds(0);
    sf::Time lag = sf::milliseconds(0);

    while (m_running.load()) {
        lag += (elapsed = loopClock.restart());
//        LDEBUG(lag.asMilliseconds());

        // Process window events end clear screen
        if (m_window) {
            sf::Event event;
            while (m_window->pollEvent(event)) {
                m_eventManager->sendEvent(createSfmlEvent(event));
            }       
        }

        // Process game events
        m_eventManager->processEvents();

        // Update scheduler
        m_scheduler->update(elapsed);

        // Update tick
        while (lag >= m_tickTime) {
            tickFunc(m_tickTime);
            lag -= m_tickTime;
        }

        if (m_window) {
            m_window->clear();

            // Draw scene
            if (m_scene)
                m_scene->draw(*m_window, sf::RenderStates(), elapsed);

            // Draw gui
            m_guiManager->draw(*m_window, sf::RenderStates(), elapsed);

            // Display window
            m_window->display();
        }
    }
}

void Game::tickFunc(sf::Time elapsed)
{
    // Update scene
    if (m_scene)
        m_scene->update(elapsed);
}

GuiManager *Game::guiManager() const
{
    return m_guiManager.get();
}

std::shared_ptr<Scene> Game::scene() const
{
    return m_scene;
}

void Game::setScene(const std::shared_ptr<Scene> &scene)
{
    m_scene = scene;
}

ResourceManager *Game::resourceManager() const
{
    return m_resourceManager.get();
}

Scheduler *Game::scheduler() const
{
    return m_scheduler.get();
}

std::shared_ptr<InputBinder> Game::inputBinder() const
{
    return m_inputBinder;
}

std::string Game::version() const
{
    return m_version;
}

void Game::setVersion(const std::string &version)
{
    m_version = version;
}

EventManager *Game::eventManager() const
{
    return m_eventManager.get();
}

void Game::setInputBinder(const std::shared_ptr<InputBinder> &inputBinder)
{
    if (!inputBinder)
        return;
    if (m_inputBinder) {
        m_inputBinder->setIgnoreEvent(true);
        m_inputBinder.reset();
    }
    m_inputBinder = inputBinder;
    m_inputBinder->setIgnoreEvent(false);
}

std::string Game::author() const
{
    return m_author;
}

void Game::setAuthor(const std::string &author)
{
    m_author = author;
}

std::string Game::name() const
{
    return m_name;
}

void Game::setName(const std::string &name)
{
    m_name = name;
}

}
