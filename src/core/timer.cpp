#include <sgen/core/timer.h>
#include <sgen/core/game.h>
#include <sgen/core/scheduler.h>

namespace sgen {

struct IdDeleter {
    void operator()(Id *id) {
        Game::instance()->scheduler()->removeTimer(*id);
        delete id;
    }
};

Timer::Timer()
{
    Id id = Game::instance()->scheduler()->createTimer(sf::milliseconds(1000),
                                                       false,
                                                       std::bind(&Timer::timeoutCallback, this));
    m_id = std::shared_ptr<Id>(new Id(id), IdDeleter());
}

Timer::~Timer()
{
    Game::instance()->scheduler()->removeTimer(*m_id);
}

bool Timer::start()
{
    return Game::instance()->scheduler()->startTimer(*m_id);
}

bool Timer::stop()
{
    return Game::instance()->scheduler()->stopTimer(*m_id);
}

bool Timer::isActive() const
{
    return Game::instance()->scheduler()->isTimerActive(*m_id);
}

sf::Time Timer::interval() const
{
    return Game::instance()->scheduler()->timerInerval(*m_id);
}

void Timer::setInterval(const sf::Time &interval)
{
    Game::instance()->scheduler()->setTimerInterval(*m_id, interval);
}

bool Timer::singleShot() const
{
    return Game::instance()->scheduler()->isTimerSingleShot(*m_id);
}

void Timer::setSingleShot(bool singleShot)
{
    Game::instance()->scheduler()->setTimerSingleShot(*m_id, singleShot);
}

void Timer::timeoutCallback()
{
    timeout();
}

}
