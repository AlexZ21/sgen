#include <sgen/core/eventhandler.h>
#include <sgen/core/game.h>
#include <sgen/core/eventmanager.h>

#include <algorithm>

namespace sgen {

EventHandler::EventHandler(const std::vector<int32_t> &eventFilter, int32_t eventPriority) :
      m_eventPriority(eventPriority),
      m_eventFilter(eventFilter),
      m_ignoreEvent(false)
{
    if (Game::instance() && Game::instance()->eventManager())
        Game::instance()->eventManager()->addEventHandler(this);
}

EventHandler::~EventHandler()
{
    if (Game::instance() && Game::instance()->eventManager())
        Game::instance()->eventManager()->removeEventHandler(this);
}

bool EventHandler::event(const Event *event)
{
    (void)(event);
    return false;
}

int32_t EventHandler::eventPriority() const
{
    return m_eventPriority;
}

std::vector<int32_t> EventHandler::eventFilter() const
{
    return m_eventFilter;
}

bool EventHandler::filterEvent(int32_t type) const
{
    return std::find(m_eventFilter.begin(), m_eventFilter.end(), type) != m_eventFilter.end();
}

bool EventHandler::ignoreEvent() const
{
    return m_ignoreEvent;
}

void EventHandler::setIgnoreEvent(bool ignore)
{
    m_ignoreEvent = ignore;
}

}
