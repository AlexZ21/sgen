#include <sgen/core/entity.h>
#include <sgen/core/world.h>
#include <sgen/common/log.h>

namespace sgen {

Entity::Entity()
{

}

Entity::Entity(World *world, Id id) :
      m_world(world),
      m_id(id)
{
}

Entity::~Entity()
{

}

World *Entity::world() const
{
    return m_world;
}

bool Entity::isValid() const
{
    if (!m_world)
        return false;
    return m_world->isValid(*this);
}

bool Entity::isEnabled() const
{
    if (!m_world)
        return false;
    return m_world->isEnabled(*this);
}

void Entity::enable()
{
    if (!m_world)
        return;
    return m_world->enableEntity(*this);
}

void Entity::disable()
{
    if (!m_world)
        return;
    return m_world->disableEntity(*this);
}

void Entity::destroy()
{
    if (!m_world)
        return;
    return m_world->destroyEntity(*this);
}

void Entity::removeAllComponents()
{
    if (!m_world)
        return;
    m_world->removeAllComponents(*this);
}

std::vector<Component *> Entity::components() const
{
    if (!m_world)
        return std::vector<Component *>();
    return m_world->components(*this);
}

ComponentMask Entity::componentTypeList() const
{
    if (!m_world)
        return ComponentMask();
    return m_world->componentMask(*this);
}

bool Entity::operator==(const Entity &entity) const
{
    return m_id == entity.m_id && entity.m_world == m_world;
}

void Entity::addComponent(Component *component, TypeId componentTypeId)
{
    if (!m_world)
        return;
    bool oldState = isEnabled();
    disable();
    m_world->addComponent(*this, component, componentTypeId);
    if (oldState)
        enable();
}

void Entity::removeComponent(TypeId componentTypeId)
{
    if (!m_world)
        return;
    bool oldState = isEnabled();
    disable();
    m_world->removeComponent(*this, componentTypeId);
    if (oldState)
        enable();
}

Component *Entity::component(TypeId componentTypeId) const
{
    if (!m_world)
        return nullptr;
    return m_world->component(*this, componentTypeId);
}

bool Entity::hasComponent(TypeId componentTypeId) const
{
    if (!m_world)
        return false;
    return m_world->hasComponent(*this, componentTypeId);
}

}
