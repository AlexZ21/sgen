#include <sgen/core/resourcemanager.h>

#include <fstream>

namespace sgen {

void ResourceManager::load(const std::string &filePath)
{
    nlohmann::json json;

    try {
        std::ifstream ifs(filePath);
        ifs >> json;

        if (json.is_array()) {
            for (auto &element : json) {
                if (element.is_object()) {
                    try {
                        ResourceType type = element["resource_type"].get<int32_t>();
                        IResourceCache *c = cache(type);
                        if (c)
                            c->load(element);
                    } catch (const std::exception &e) {
                        LWARN("Error loading resource from json: ", e.what());
                    }
                }
            }

        } else if (json.is_object()) {
            try {
                ResourceType type = json["resource_type"].get<int32_t>();
                IResourceCache *c = cache(type);
                if (c)
                    c->load(json);
            } catch (const std::exception &e) {
                LWARN("Error loading resource from json: ", e.what());
            }
        }
    } catch (const std::exception &e) {
        LWARN("Error load theme json: ", e.what());
    }
}

}
