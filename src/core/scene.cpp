#include <sgen/core/scene.h>
#include <sgen/systems/scenesystem.h>
#include <sgen/components/scenecomponent.h>

#include <queue>

namespace sgen {

struct ZIndex
{
    bool operator() (const ZIndex &a, const ZIndex &b) const {
        return a.drawable->zIndex() > b.drawable->zIndex();
    }
    Drawable *drawable;
};

Scene::Scene() :
      World()
{
    addSystem<SceneSystem>();
    m_sceneSystem = system<SceneSystem>();
}

void Scene::update(sf::Time elapsed)
{
    // Refresh world
    refresh();

    // Update all systems
    for (auto &system : systems()) {
        system->update(elapsed.asMilliseconds());
    }

}

void Scene::draw(sf::RenderTarget &target, sf::RenderStates states, sf::Time elapsed)
{
    const sf::View &view = target.getView();
    sf::Vector2f viewPos = view.getCenter();
    sf::Vector2f viewSize = view.getSize();
    sf::FloatRect viewportRect(viewPos.x - viewSize.x / 2, viewPos.y - viewSize.y / 2,
                               viewSize.x, viewSize.y);

    static std::priority_queue<ZIndex, std::vector<ZIndex>, ZIndex> renderQueue;

    m_sceneSystem->entityTree().queryOverlaps(viewportRect,
                                              [](const Entity &e){
                                                  SceneComponent *sc = e.component<SceneComponent>();
                                                  if (sc) {
                                                      Drawable *drawable = sc->drawable().get();
                                                      if (drawable) {
                                                          ZIndex zindex = {drawable};
                                                          renderQueue.push(zindex);
                                                      }
                                                  }
                                              });

    while (!renderQueue.empty()) {
        ZIndex zindex = renderQueue.top();
        sf::RenderStates s = states;
//        target.draw(*zindex.drawable, s);
        zindex.drawable->draw(target, s, elapsed);
        renderQueue.pop();
    }
}

}
