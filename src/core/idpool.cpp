#include <sgen/core/idpool.h>

namespace sgen {

IdPool::IdPool() :
    m_indexCounter(0)
{
}

Id IdPool::create()
{
    if (!m_freeList.empty()) {
        uint32_t index = m_freeList.back();
        m_freeList.pop_back();
        return Id(index, m_versions[index]);
    } else {
        uint32_t index = m_indexCounter++;
        m_versions.resize(m_indexCounter);
        m_versions[index] = 1;
        return Id(index, m_versions[index]);
    }
}

void IdPool::remove(const Id &id)
{
    if (!valid(id))
        return;
    uint32_t &version = m_versions[id];
    ++version;
    m_freeList.push_back(id.index());
}

Id IdPool::get(uint32_t index) const
{
    if (index >= m_versions.size())
        return INVALID_ID;
    return Id(index, m_versions[index]);
}

bool IdPool::valid(const Id &id) const
{
    return id.index() < m_versions.size() && id.version() == m_versions[id];
}

int32_t IdPool::size() const
{
    return static_cast<int32_t>(m_versions.size());
}

void IdPool::clear()
{
    m_versions.clear();
    m_freeList.clear();
    m_indexCounter = 0;
}

}
